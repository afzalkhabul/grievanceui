
// var uploadFileURL='http://139.162.27.78:8869/api/Uploads/dhanbadDb/download/';
var uploadFileURL='http://localhost:3004/api/Uploads/dhanbadDb/download/';

angular.module('ui.tinymce', ['ui.calendar'])
    .value('uiTinymceConfig', {})
    .directive('uiTinymce', ['uiTinymceConfig', function(uiTinymceConfig) {
        uiTinymceConfig = uiTinymceConfig || {};
        var generatedIds = 0;
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ngModel) {
                var expression, options, tinyInstance;
                // generate an ID if not present
                if (!attrs.id) {
                    attrs.$set('id', 'uiTinymce' + generatedIds++);
                }
                options = {
                    // Update model when calling setContent (such as from the source editor popup)
                    setup: function(ed) {
                        ed.on('init', function(args) {
                            ngModel.$render();
                        });
                        // Update model on button click
                        ed.on('ExecCommand', function(e) {
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                        // Update model on keypress
                        ed.on('KeyUp', function(e) {
                            console.log(ed.isDirty());
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                    },
                    mode: 'exact',
                    elements: attrs.id,
                };
                if (attrs.uiTinymce) {
                    expression = scope.$eval(attrs.uiTinymce);
                } else {
                    expression = {};
                }
                angular.extend(options, uiTinymceConfig, expression);
                setTimeout(function() {
                    tinymce.init(options);
                });
                ngModel.$render = function() {
                    if (!tinyInstance) {
                        tinyInstance = tinymce.get(attrs.id);
                    }
                    if (tinyInstance) {
                        tinyInstance.setContent(ngModel.$viewValue || '');
                    }
                };
            }
        };
    }]);

var app = angular.module('myApp', ['ngRoute','angular-page-loader','workflow','project','legalMgmt','landAndAsset',  'ui.calendar','ngCalendar', 'ngFileUpload', 'datatables','angularjs-dropdown-multiselect', 'ui.tinymce', 'datatables.bootstrap', 'servicesDetails']);

app.factory('httpRequestInterceptor', function ($window,$q,$location) {
    return {
        request: function (config) {
            if($window.localStorage.getItem('tokenId')) {
                config.headers['access_token'] = $window.localStorage.getItem('tokenId');
            }
            return config;
        },
        response: function(response) {
            var deferred = $q.defer();
            deferred.resolve(response);
            return deferred.promise;
        },
        responseError: function (rejection) {
            if(rejection.status === 401) {
                console.log('reject responce '+JSON.stringify(rejection))
                //if(rejection."Unauthorized")
                $window.localStorage.clear();
                location.reload();
             /*   if(rejection.data.error.message=="Authorization Required"){

                }*/

            }if(rejection.status === 500 || rejection.status === 403){
                var errorUrlData=$location.host()+':3015/error';
                location.href="http://"+$location.host()+":3015/error";
                return false;
            }
            return $q.reject(rejection);
        }
    };
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpRequestInterceptor');
});


app.directive('validNumber', function() {
    return {
        require: '?ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            if(!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function(val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split ('.');
                if(!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean =negativeCheck[0] + '-' + negativeCheck[1];
                    if(negativeCheck[0].length > 0) {
                        clean =negativeCheck[0];
                    }
                }
                if(!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0,2);
                    clean =decimalCheck[0] + '.' + decimalCheck[1];
                }
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function(event) {
                if(event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.config(function($routeProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: './home.html',
            controller: 'homeController'
        }).when('/schemes', {
            templateUrl: './schemes.html',
            controller: 'schemesController'
        }).when('/addScheme', {
            templateUrl: './addScheme.html',
            controller: 'addSchemeController'
        }).when('/editScheme/:packId', {
            templateUrl: './editScheme.html',
            controller: 'editSchemeController'
        }).when('/viewScheme/:packId', {
            templateUrl: './viewScheme.html',
            controller: 'viewSchemeController'
        }).when('/viewRequest/:requstId', {
            templateUrl: './viewRequest.html',
            controller: 'viewRequestController'
        }).when('/requestReports', {
            templateUrl: './viewRequest.html',
            controller: 'viewRequestController'
        }).when('/changeStateRequest/:requstId', {
            templateUrl: './editRequest.html',
            controller: 'editRequestController'
        }).when('/requests', {
            templateUrl: './requests.html',
            controller: 'requestsController'
        }).when('/schemeForms', {
            templateUrl: './schemeForms.html',
            controller: 'schemeFormsController'
        }).when('/createSchemeForms', {
            templateUrl: './createSchemeForms.html',
            controller: 'createSchemeFormsController'
        }).when('/viewSchemeForms/:formId', {
            templateUrl: './viewSchemeForms.html',
            controller: 'viewSchemeFormsController'
        }).when('/userProfile', {
            templateUrl: './userProfile.html',
            controller: 'userProfileController'
        }).when('/beneficiariesList', {
            templateUrl: './beneficiariesList.html',
            controller: 'beneficiariesListController'
        }).when('/ministriesList', {
            templateUrl: './ministriesList.html',
            controller: 'ministriesListController'
        }).when('/schemeDepartment', {
            templateUrl: './schemeDepartment.html',
            controller: 'departmentController'
        }).when('/login', {
            templateUrl: './login.html',
            controller: 'loginController'
        }) .when('/grievanceModulelogin/:employeeId', {
            templateUrl: './blank.html',
            controller: 'schemeModuleController'
        }).otherwise({redirectTo: '/'});

});



var directiveModule = angular.module('angularjs-dropdown-multiselect', []);
directiveModule.directive('ngDropdownMultiselect', ['$filter', '$document', '$compile', '$parse',

    function ($filter, $document, $compile, $parse) {
        return {
            restrict: 'AE',
            scope: {
                selectedModel: '=',
                options: '=',
                extraSettings: '=',
                events: '=',
                searchFilter: '=?',
                translationTexts: '=',
                groupBy: '@'
            },
            template: function (element, attrs) {
                var checkboxes = attrs.checkboxes ? true : false;
                var groups = attrs.groupBy ? true : false;
                var template = '<div class="multiselect-parent btn-group dropdown-multiselect">';
                template += '<button type="button" class="dropdown-toggle" ng-class="settings.buttonClasses" ng-click="toggleDropdown()">{{getButtonText()}}&nbsp;<span class="caret"></span></button>';
                template += '<ul class="dropdown-menu dropdown-menu-form" ng-style="{display: open ? \'block\' : \'none\', height : settings.scrollable ? settings.scrollableHeight : \'auto\' }" style="overflow: scroll" >';
                template += '<li ng-hide="(!settings.showCheckAll || settings.selectionLimit > 0) && !settings.showUncheckAll" class="divider"></li>';
                template += '<li class="abc1 " ng-show="settings.enableSearch"><div class="dropdown-header"><input type="text" class="form-control" style="width: 100%;" ng-model="searchFilter" placeholder="{{texts.searchPlaceholder}}" /></li>';
                template += '<li class="abc2 " ng-hide="!settings.showCheckAll || settings.selectionLimit > 0"><a data-ng-click="selectAll()"><span class="glyphicon glyphicon-ok"></span>  {{texts.checkAll}}</a></li>';
                template += '<li class="abc2 " ng-show="settings.showUncheckAll"><a data-ng-click="deselectAll();"><span class="glyphicon glyphicon-remove"></span>   {{texts.uncheckAll}}</a></li>';
                template += '<li class="abc3 "><button type="button" class="btn btn-success dropdown-toggle" ng-class="settings.buttonClasses" ng-click="toggleDropdown()">OK</button></li>';
                template += '<li ng-show="settings.enableSearch" class="divider"></li>';
                if (groups) {
                    template += '<li ng-repeat-start="option in orderedItems | filter: searchFilter" ng-show="getPropertyForObject(option, settings.groupBy) !== getPropertyForObject(orderedItems[$index - 1], settings.groupBy)" role="presentation" class="dropdown-header">{{ getGroupTitle(getPropertyForObject(option, settings.groupBy)) }}</li>';
                    template += '<li ng-repeat-end role="presentation">';
                } else {
                    template += '<li role="presentation" ng-repeat="option in options | filter: searchFilter">';
                }
                template += '<a role="menuitem" tabindex="-1" ng-click="setSelectedItem(getPropertyForObject(option,settings.idProp))">';
                if (checkboxes) {
                    template += '<div class="checkbox"><label><input class="checkboxInput" type="checkbox" ng-click="checkboxClick($event, getPropertyForObject(option,settings.idProp))" ng-checked="isChecked(getPropertyForObject(option,settings.idProp))" /> {{getPropertyForObject(option, settings.displayProp)}}</label></div></a>';
                } else {
                    template += '<span data-ng-class="{\'glyphicon glyphicon-ok\': isChecked(getPropertyForObject(option,settings.idProp))}"></span> {{getPropertyForObject(option, settings.displayProp)}}</a>';
                }
                template += '</li>';
                template += '<li class="divider" ng-show="settings.selectionLimit > 1"></li>';
                template += '<li role="presentation" ng-show="settings.selectionLimit > 1"><a role="menuitem">{{selectedModel.length}} {{texts.selectionOf}} {{settings.selectionLimit}} {{texts.selectionCount}}</a></li>';
                template += '</ul>';
                template += '</div>';
                element.html(template);
            },
            link: function ($scope, $element, $attrs) {
                var $dropdownTrigger = $element.children()[0];
                $scope.toggleDropdown = function () {
                    $scope.open = !$scope.open;
                };
                $scope.checkboxClick = function ($event, id) {
                    $scope.setSelectedItem(id);
                    $event.stopImmediatePropagation();
                };
                $scope.externalEvents = {
                    onItemSelect: angular.noop,
                    onItemDeselect: angular.noop,
                    onSelectAll: angular.noop,
                    onDeselectAll: angular.noop,
                    onInitDone: angular.noop,
                    onMaxSelectionReached: angular.noop
                };
                $scope.settings = {
                    dynamicTitle: true,
                    scrollable: true,
                    scrollableHeight: '200px',
                    closeOnBlur: true,
                    displayProp: 'label',
                    idProp: 'id',
                    externalIdProp: 'id',
                    enableSearch: true,
                    selectionLimit: 0,
                    showCheckAll: true,
                    showUncheckAll: true,
                    closeOnSelect: false,
                    buttonClasses: 'btn btn-default',
                    closeOnDeselect: false,
                    groupBy: $attrs.groupBy || undefined,
                    groupByTextProvider: null,
                    smartButtonMaxItems: 0,
                    smartButtonTextConverter: angular.noop
                };

                $scope.texts = {
                    checkAll: 'Check All',
                    uncheckAll: 'Uncheck All',
                    selectionCount: 'checked',
                    selectionOf: '/',
                    searchPlaceholder: 'Search...',
                    buttonDefaultText: 'Select',
                    dynamicButtonTextSuffix: 'checked'
                };

                $scope.searchFilter = $scope.searchFilter || '';

                if (angular.isDefined($scope.settings.groupBy)) {
                    $scope.$watch('options', function (newValue) {
                        if (angular.isDefined(newValue)) {
                            $scope.orderedItems = $filter('orderBy')(newValue, $scope.settings.groupBy);
                        }
                    });
                }

                angular.extend($scope.settings, $scope.extraSettings || []);
                angular.extend($scope.externalEvents, $scope.events || []);
                angular.extend($scope.texts, $scope.translationTexts);

                $scope.singleSelection = $scope.settings.selectionLimit === 1;

                function getFindObj(id) {
                    var findObj = {};

                    if ($scope.settings.externalIdProp === '') {
                        findObj[$scope.settings.idProp] = id;
                    } else {
                        findObj[$scope.settings.externalIdProp] = id;
                    }

                    return findObj;
                }

                function clearObject(object) {
                    for (var prop in object) {
                        delete object[prop];
                    }
                }

                if ($scope.singleSelection) {
                    if (angular.isArray($scope.selectedModel) && $scope.selectedModel.length === 0) {
                        clearObject($scope.selectedModel);
                    }
                }

                if ($scope.settings.closeOnBlur) {
                    $document.on('click', function (e) {
                        var target = e.target.parentElement;
                        var parentFound = false;

                        while (angular.isDefined(target) && target !== null && !parentFound) {
                            if (_.contains(target.className.split(' '), 'multiselect-parent') && !parentFound) {
                                if (target === $dropdownTrigger) {
                                    parentFound = true;
                                }
                            }
                            target = target.parentElement;
                        }

                        if (!parentFound) {
                            $scope.$apply(function () {
                                $scope.open = false;
                            });
                        }
                    });
                }

                $scope.getGroupTitle = function (groupValue) {
                    if ($scope.settings.groupByTextProvider !== null) {
                        return $scope.settings.groupByTextProvider(groupValue);
                    }

                    return groupValue;
                };

                $scope.getButtonText = function () {
                    if ($scope.settings.dynamicTitle && ($scope.selectedModel.length > 0 || (angular.isObject($scope.selectedModel) && _.keys($scope.selectedModel).length > 0))) {
                        if ($scope.settings.smartButtonMaxItems > 0) {
                            var itemsText = [];

                            angular.forEach($scope.options, function (optionItem) {
                                if ($scope.isChecked($scope.getPropertyForObject(optionItem, $scope.settings.idProp))) {
                                    var displayText = $scope.getPropertyForObject(optionItem, $scope.settings.displayProp);
                                    var converterResponse = $scope.settings.smartButtonTextConverter(displayText, optionItem);

                                    itemsText.push(converterResponse ? converterResponse : displayText);
                                }
                            });

                            if ($scope.selectedModel.length > $scope.settings.smartButtonMaxItems) {
                                itemsText = itemsText.slice(0, $scope.settings.smartButtonMaxItems);
                                itemsText.push('...');
                            }

                            return itemsText.join(', ');
                        } else {
                            var totalSelected;

                            if ($scope.singleSelection) {
                                totalSelected = ($scope.selectedModel !== null && angular.isDefined($scope.selectedModel[$scope.settings.idProp])) ? 1 : 0;
                            } else {
                                totalSelected = angular.isDefined($scope.selectedModel) ? $scope.selectedModel.length : 0;
                            }

                            if (totalSelected === 0) {
                                return $scope.texts.buttonDefaultText;
                            } else {
                                return totalSelected + ' ' + $scope.texts.dynamicButtonTextSuffix;
                            }
                        }
                    } else {
                        return $scope.texts.buttonDefaultText;
                    }
                };

                $scope.getPropertyForObject = function (object, property) {
                    if (angular.isDefined(object) && object.hasOwnProperty(property)) {
                        return object[property];
                    }

                    return '';
                };

                $scope.selectAll = function () {
                    $scope.deselectAll(false);
                    $scope.externalEvents.onSelectAll();

                    angular.forEach($scope.options, function (value) {
                        $scope.setSelectedItem(value[$scope.settings.idProp], true);
                    });
                };

                $scope.deselectAll = function (sendEvent) {
                    sendEvent = sendEvent || true;

                    if (sendEvent) {
                        $scope.externalEvents.onDeselectAll();
                    }

                    if ($scope.singleSelection) {
                        clearObject($scope.selectedModel);
                    } else {
                        $scope.selectedModel.splice(0, $scope.selectedModel.length);
                    }
                };

                $scope.setSelectedItem = function (id, dontRemove) {
                    var findObj = getFindObj(id);
                    var finalObj = null;

                    if ($scope.settings.externalIdProp === '') {
                        finalObj = _.find($scope.options, findObj);
                    } else {
                        finalObj = findObj;
                    }

                    if ($scope.singleSelection) {
                        clearObject($scope.selectedModel);
                        angular.extend($scope.selectedModel, finalObj);
                        $scope.externalEvents.onItemSelect(finalObj);
                        if ($scope.settings.closeOnSelect) $scope.open = false;

                        return;
                    }

                    dontRemove = dontRemove || false;

                    var exists = _.findIndex($scope.selectedModel, findObj) !== -1;

                    if (!dontRemove && exists) {
                        $scope.selectedModel.splice(_.findIndex($scope.selectedModel, findObj), 1);
                        $scope.externalEvents.onItemDeselect(findObj);
                    } else if (!exists && ($scope.settings.selectionLimit === 0 || $scope.selectedModel.length < $scope.settings.selectionLimit)) {
                        $scope.selectedModel.push(finalObj);
                        $scope.externalEvents.onItemSelect(finalObj);
                    }
                    if ($scope.settings.closeOnSelect) $scope.open = false;
                };

                $scope.isChecked = function (id) {
                    if ($scope.singleSelection) {
                        return $scope.selectedModel !== null && angular.isDefined($scope.selectedModel[$scope.settings.idProp]) && $scope.selectedModel[$scope.settings.idProp] === getFindObj(id)[$scope.settings.idProp];
                    }

                    return _.findIndex($scope.selectedModel, getFindObj(id)) !== -1;
                };

                $scope.externalEvents.onInitDone();
            }
        };
    }]);

app.controller('homeController', function($http, $scope, $window, $location, $rootScope) {
    $rootScope.userName = $window.localStorage.getItem('userName');
    console.log("homeController");
});

app.controller('schemesController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("schemesController");


     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #schemesLi").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
        		   if(!response){
        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption('order', [1, 'asc'])
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers');

    $scope.loadingImage=true;
    $scope.uploadURL=uploadFileURL;
    $http({
        "method": "GET",
        "url": 'api/Schemes',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        $scope.schemeLists = response;
        $scope.loadingImage=false;
    }).error(function (response, data) {
        console.log("failure");
    })

    $scope.addScheme = function() {
        location.href = '#/addScheme';
        $scope.loadingImage=false;
    }
    $scope.viewRequest = function() {
        $scope.loadingImage=false;
        location.href = '#/viewRequest';
    }
    $scope.exportToExcel=function(tableId){
        //alert(tableId);
        if( $scope.schemeLists!=null &&  $scope.schemeLists.length>0){

            $("<tr>" +
                "<th>Name</th>" +
                "<th>Category</th>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "<th>Budget</th>" +
                "<th>Commincement</th>" +
                "<th>Comment</th>" +
                "<th>Ministry Name</th>" +
                "<th>Contact One</th>" +
                "<th>Contact Two</th>" +
                "<th>Contact Three</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.schemeLists.length;i++){
                var schemes=$scope.schemeLists[i];

                $("<tr>" +
                    "<td>"+schemes.name+"</td>" +
                    "<td>"+schemes.category+"</td>" +
                    "<td>"+schemes.departmentName+"</td>" +
                    "<td>"+schemes.status+"</td>" +
                    "<td>"+schemes.budget+"</td>" +
                    "<td>"+schemes.commincement+"</td>" +
                    "<td>"+schemes.comment+"</td>" +
                    "<td>"+schemes.ministryName+"</td>" +
                    "<td><table><tr><td>employeeId"+"</td><td>"+":</td><td>"+schemes.contactOne.employeeId+"</td></tr>" +
                    "<tr><td>address"+"</td><td>"+":</td><td>"+schemes.contactOne.address+"</td></tr>"+
                    "<tr><td>name"+"</td><td>"+":</td><td>"+schemes.contactOne.name+"</td></tr>"+
                    "<tr><td>email"+"</td><td>"+":</td><td>"+schemes.contactOne.email+"</td></tr>"+
                    "<tr><td>designation"+"</td><td>"+":</td><td>"+schemes.contactOne.designation+"</td></tr>"+
                    "<tr><td>phoneOne"+"</td><td>"+":</td><td>"+schemes.contactOne.phoneOne+"</td></tr>"+
                    "<tr><td>phoneTwo"+"</td><td>"+":</td><td>"+schemes.contactOne.phoneTwo+"</td></tr>"+
                    "</table></td>" +
                    "<td><table><tr><td>employeeId"+"</td><td>"+":</td><td>"+schemes.contactTwo.employeeId+"</td></tr>" +
                    "<tr><td>address"+"</td><td>"+":</td><td>"+schemes.contactTwo.address+"</td></tr>"+
                    "<tr><td>name"+"</td><td>"+":</td><td>"+schemes.contactTwo.name+"</td></tr>"+
                    "<tr><td>email"+"</td><td>"+":</td><td>"+schemes.contactTwo.email+"</td></tr>"+
                    "<tr><td>designation"+"</td><td>"+":</td><td>"+schemes.contactTwo.designation+"</td></tr>"+
                    "<tr><td>phoneOne"+"</td><td>"+":</td><td>"+schemes.contactTwo.phoneOne+"</td></tr>"+
                    "<tr><td>phoneTwo"+"</td><td>"+":</td><td>"+schemes.contactTwo.phoneTwo+"</td></tr>"+
                    "</table></td>" +
                    "<td><table><tr><td>employeeId"+"</td><td>"+":</td><td>"+schemes.contactThree.employeeId+"</td></tr>" +
                    "<tr><td>address"+"</td><td>"+":</td><td>"+schemes.contactThree.address+"</td></tr>"+
                    "<tr><td>name"+"</td><td>"+":</td><td>"+schemes.contactThree.name+"</td></tr>"+
                    "<tr><td>email"+"</td><td>"+":</td><td>"+schemes.contactThree.email+"</td></tr>"+
                    "<tr><td>designation"+"</td><td>"+":</td><td>"+schemes.contactThree.designation+"</td></tr>"+
                    "<tr><td>phoneOne"+"</td><td>"+":</td><td>"+schemes.contactThree.phoneOne+"</td></tr>"+
                    "<tr><td>phoneTwo"+"</td><td>"+":</td><td>"+schemes.contactThree.phoneTwo+"</td></tr>"+
                    "</table></td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Lists of Schemes ');
            $timeout(function () { location.href = exportHref; }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }

    }

}]);

app.factory('Excel',function($window){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
});

app.controller('requestsController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("requestsController");

    $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #schemesLi").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.requests").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.requests .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeRequests'}, function (response) {
//            		alert(JSON.stringify(response));
            		   if(!response){
            			window.location.href = "#/noAccessPage";
            		   }
            		});

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.uploadURL=uploadFileURL;
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.viewRequestDiv=true;
    $scope.viewRequestDivBack=true;
    $scope.requestDivBack=false;
    $scope.requestDiv=false;
    $scope.editRequestDivBack=false;
    $scope.editRequestDiv=false;
    var employeeId=$scope.userInfo.employeeId;
    var employeeName=$scope.userInfo.name;
    $scope.getRequest=function () {
        $http({
            "method": "GET",
            "url": 'api/Requests/getDetails?employeeId=%7B%22employeeId%22%3A%22'+employeeId+'%22%7D',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response, data) {
            console.log("request details"+ JSON.stringify(response.data));
            $scope.requestLists = response.data;
            $scope.loadingImage=false;
        }).error(function (response, data) {
            console.log("failure");
        })

    }
    $scope.getRequest();
    $scope.changeStatus=function (id) {
        if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
            for(var i=0;i<$scope.requestLists.length;i++){
                if(id==$scope.requestLists[i].id){
                    $scope.editRequestDetails=angular.copy($scope.requestLists[i]) ;
                    $scope.viewRequestDiv=false;
                    $scope.viewRequestDivBack=false;
                    $scope.editRequestDivBack=true;
                    $scope.editRequestDiv=true;
                    $scope.requestDivBack=false;
                    $scope.requestDiv=false;
                }
            }
        }
    }
    $scope.viewStatus=function (id) {
        if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
            for(var i=0;i<$scope.requestLists.length;i++){
                if(id==$scope.requestLists[i].id){
                    $scope.viewRequestDetails=angular.copy($scope.requestLists[i]) ;
                    $scope.viewRequestDiv=false;
                    $scope.viewRequestDivBack=false;
                    $scope.editRequestDivBack=false;
                    $scope.editRequestDiv=false;
                    $scope.requestDivBack=true;
                    $scope.requestDiv=true;
                }
            }
        }
    }
    $scope.clickToBack = function(){
        $window.location.reload();
    }

    $scope.acceptStatus = function (request) {

        var updateDetails = {
            'email':request.email,
            "acceptLevel":request.acceptLevel,
            "acceptStatus": "Yes",
            "requestStatus": "Approval",
            "comment": request.comment,
            "requestId":request.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };

        $scope.approvalScheme = true;

        $http({
            "method": "POST",
            "url": 'api/Requests/updateDetails',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            console.log("filter Schemes "+ JSON.stringify(response));
            $scope.requestLists = JSON.stringify(response);
            $window.localStorage.setItem('requestFlag',true);
            $("#acceptStatus").modal("show");
            /*setTimeout(function(){$('#acceptStatus').modal('hide')}, 3000);*/
            /*$window.location.reload();*/
            //$window.localStorage.getItem('requestTab');

        }).error(function (response, data) {
            console.log("failure");
        })
    }
    $scope.acceptStatusModal = function() {
        $window.location.reload();
    }
    $scope.rejectStatusModal = function() {
        $window.location.reload();
    }
    $scope.rejectStatus = function (request) {
        var updateDetails = {
            'email':request.email,
            "acceptLevel":request.acceptLevel,
            "acceptStatus": "No",
            "requestStatus": "Rejected",
            "comment": request.comment,
            "requestId":request.id,
            "employeeId":employeeId,
            "employeeName":employeeName
        };
        $http({
            "method": "POST",
            "url": 'api/Requests/updateDetails',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            $scope.requestLists = response.data;
            $window.localStorage.setItem('requestFlag',true);
            $("#rejectStatus").modal("show");
        }).error(function (response, data) {
            console.log("failure");
        })
    }

    $scope.exportToExcel=function(tableId){
        if( $scope.requestLists!=null &&  $scope.requestLists.length>0){

            $("<tr>" +
                "<th>requestId</th>" +
                "<th>createdTime</th>" +
                "<th>schemeName</th>" +
                "<th>name</th>" +
                "<th>emailId</th>" +
                "<th>mobileNumber</th>" +
                "<th>requestStatus</th>" +
                "<th>acceptStatus</th>" +
                "<th>acceptLevel</th>" +
                "<th>maxlevel</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.requestLists.length;i++){
                var request=$scope.requestLists[i];

                $("<tr>" +
                    "<td>"+request.requestId+"</td>" +
                    "<td>"+request.createdTime+"</td>" +
                    "<td>"+request.schemeName+"</td>" +
                    "<td>"+request.name+"</td>" +
                    "<td>"+request.emailId+"</td>" +
                    "<td>"+request.mobileNumber+"</td>" +
                    "<td>"+request.requestStatus+"</td>" +
                    "<td>"+request.acceptStatus+"</td>" +
                    "<td>"+request.acceptLevel+"</td>" +
                    "<td>"+request.maxlevel+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Requests ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }


    }

    $scope.viewRequest = function() {
        $scope.loadingImage=false;
        location.href = '#/viewRequest';
    }
    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

}]);

app.controller('addSchemeController', ['schemeManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$routeParams', function(schemeManagement, $http, $scope,Upload, $timeout, $window, $location, $rootScope, $routeParams) {
    console.log("createEditSchemeController");

    $(document).ready(function () {
             $('html,body').scrollTop(0);
             $(".sidebar-menu li ul > li").removeClass('active1');
             $('[data-toggle="tooltip"]').tooltip();
             $(".sidebar-menu #schemesLi").addClass("active");
             $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
             $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
             $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes").addClass("active1").siblings('.active1').removeClass('active1');
             $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes .collapse").addClass("in");
             $(".sidebar-menu li .collapse").removeClass("in");
         });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.uploadURL=uploadFileURL;
    $scope.clickToBack = function(){
        $location.url("/schemes");
    }

    $scope.getEmployeeType=function () {
        $http({
            method: 'GET',
            url: 'api/Employees?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('Users Response :' + JSON.stringify(response));
            $scope.employeeList = response;
        }).error(function (response) {
        });
    };
    $scope.getEmployeeType();

    $scope.showEMPDetails = false;

    $scope.getEmpData = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectedEMP = response[0];
            $scope.showEMPDetails = true;
        }).error(function (response) {
        });
    }

    $scope.getEmpData1 = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectedEMP1 = response[0];
            $scope.showEMPDetails1 = true;
        }).error(function (response) {
        });
    }

    $scope.getEmpData2 = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectedEMP2 = response[0];
            $scope.showEMPDetails2 = true;
        }).error(function (response) {
        });
    }
   $scope.bineficiaries=[];
    $scope.bineficiariesList=[];
    $scope.getBeneficiaries = function() {
        $http({
            method: 'GET',
            url: 'api/beneficiariesLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response) {
            $scope.beneficiariesLists = response;
            if($scope.beneficiariesLists!=undefined && $scope.beneficiariesLists !=null && $scope.beneficiariesLists.length>0 ){
                for(var i=0;i<$scope.beneficiariesLists.length;i++ ){
                    var data={
                        label:$scope.beneficiariesLists[i].name,
                        id:{id: $scope.beneficiariesLists[i].id,
                            label: $scope.beneficiariesLists[i].name}
                    }
                    $scope.bineficiariesList.push(data);
                }
            }
        }).error(function (response) {
        });
    };
    $scope.getBeneficiaries();

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/Departments?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.departmentList = response;
        }).error(function (response) {
        });
    };
    $scope.getDepartments();
    $scope.example15settings = {enableSearch: true};
    $scope.getMinistries = function() {
        $http({
            method: 'GET',
            url: 'api/ministeriesLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response) {
            $scope.ministriesLists = response;
        }).error(function (response) {
        });
    };
    $scope.getMinistries();

    var filedetails=[];
    var fileIdsArray=[];
    $scope.uploadFileURL = [];
    var fileUploadStatus=true;

    $scope.uploadFiles = function (files) {
        var count=1;
        filedetails=[];
        fileIdsArray=[];
        var fileCount = 0;
       angular.forEach(files, function(file) {
            $scope.disable = true;
            $scope.errorMssg1 = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                fileUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename
                        }
                        fileIdsArray.push(fileDetails);
                        $scope.uploadFileURL.push(fileDetails);
                        filedetails.push(response.data);
                        fileUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
            if (fileCount == files.length) {
                $scope.uploadNitif = true;
                $scope.disable = false;
                $scope.errorMssg1 = false;
                $scope.formNotSelected = '';
            }
        });
    };
    var formIdsArray=[];
    $scope.uploadFormsNotif = [];
    var formUploadStatus=true;
    $scope.uploadForms = function (files) {
        formIdsArray=[];
        var fileCount = 0;
        angular.forEach(files, function(file) {
            $scope.disable = true;
            $scope.errorMssg = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename
                        }
                        formIdsArray.push(fileDetails);
                        $scope.uploadFormsNotif.push(fileDetails);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
            if (fileCount == files.length) {
                $scope.uploadFormNotif = true;
                $scope.disable = false;
                $scope.errorMssg = false;
                $scope.formNotSelected = '';
            }
        });
    };
    $scope.reset=function () {
        $scope.scheme={};
        $scope.uploadFormsNotif=[];
        $scope.uploadFileURL=[];
        $scope.bineficiaries=[];
        $scope.showEMPDetails = false;
                $scope.showEMPDetails1 = false;
                $scope.showEMPDetails2 = false;
    };
    $scope.scheme = {};
      var formRequestSubmit = true;
    $scope.createScheme= function () {
        $scope.schemeMessage="";
        $scope.schemeMessageError=false;
        if(formRequestSubmit=true) {
            formRequestSubmit = false;
            if ($scope.bineficiaries != undefined && $scope.bineficiaries != null && $scope.bineficiaries.length > 0) {

                if (fileUploadStatus && formUploadStatus) {
                    if ($scope.uploadFileURL.length > 0) {
                        if ($scope.uploadFormsNotif.length > 0) {
                            if ($scope.scheme.commincement) {
                                var beneficals = [];
                                if ($scope.bineficiaries != null && $scope.bineficiaries.length > 0) {
                                    for (var i = 0; i < $scope.bineficiaries.length; i++) {
                                        beneficals.push($scope.bineficiaries[i].id.id)
                                    }
                                }
                                            var schemeDetails = $scope.scheme;
                                            schemeDetails.file = $scope.uploadFileURL;
                                            schemeDetails.forms = $scope.uploadFormsNotif;
                                            schemeDetails.status = 'Active';
                                            schemeDetails.bineficiaries = beneficals;
                                            $scope.schemeDetailsAre = schemeDetails;
                                           $http({
                                                method: 'POST',
                                                url: 'api/Schemes',
                                                headers: {"Content-Type": "application/json", "Accept":"application/json"},
                                                "data": $scope.schemeDetailsAre
                                            }).success(function (response, data) {
                                                $('#addSchemeSuccess').modal('show');
                                                $scope.schemeDetailsAre = response;
                                                $scope.uploadFileURL = [];
                                                $scope.uploadFormsNotif = [];
                                             }).error(function (response) {

                                                formRequestSubmit = true;
                                                if (response.error.details.messages.name) {
                                                    $scope.errorMessageData = response.error.details.messages.name[0];
                                                    $scope.errorMessage = true;
                                                    $timeout(function () {
                                                        $scope.errorMessage = false;
                                                    }, 3000);
                                                }
                                            });

                            } else {
                                formRequestSubmit = true;
                                $timeout(function () {
                                    $timeout(function () {
                                        $scope.formNotSelected = "Please Select Date of Commencement";
                                    }, 500);
                                }, 500);
                            }
                        }
                        else {

                            formRequestSubmit = true;
                            $timeout(function () {
                                $timeout(function () {
                                    $scope.formNotSelected = "Please upload the required Forms";
                                }, 500);
                            }, 500);
                        }
                    } else {
                        formRequestSubmit = true;

                        $timeout(function () {
                            $timeout(function () {
                                $scope.formNotSelected = "Please upload the required Notifications";
                            }, 500);
                        }, 500);
                    }
                } else {
                    formRequestSubmit = true;
                    $scope.createScheme();
                }
            } else {
                formRequestSubmit = true;
                $timeout(function () {
                    $timeout(function () {
                        $scope.formNotSelected = "Please Select Beneficiaries";
                    }, 500);
                }, 500);
            }
        }

    };
    $scope.addSchemeSuccess = function() {
        $('#addSchemeSuccess').modal('hide');
        location.href = '#/schemes';
        $window.location.reload();
    };

    $scope.removeFiles = function(index, fileId, type){
        $http({
            method: 'DELETE',
            url: 'api/Uploads/dhanbadDb/files/'+fileId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            if(type == 'file') {
                console.log('File');
                $scope.uploadFileURL.splice(index, 1);
            }else{
                console.log('Form');
                $scope.uploadFormsNotif.splice(index, 1);
            }
        });
    };
}]);

app.controller('editSchemeController', ['schemeManagement', '$http', '$scope', 'Upload', '$timeout', '$window', '$location', '$rootScope', '$routeParams', function(schemeManagement, $http, $scope, Upload, $timeout, $window, $location, $rootScope, $routeParams) {
    console.log("editSchemeController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #schemesLi").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
    //    alert(JSON.stringify(response));
           if(!response){
            window.location.href = "#/noAccessPage";
           }
        });

    $scope.clickToBack = function(){
        $location.url("/schemes");
    }
    $scope.uploadURL=uploadFileURL;
    var filedetails=[];
    var fileIdsArray=[];
    $scope.uploadFileName = [];
    var fileUploadStatus=true;
    $scope.uploadFiles = function (files) {
        var count=1;
        filedetails=[];
        fileIdsArray=[];
        var fileCount = 0;
        angular.forEach(files, function(file) {
            $scope.disable = true;
            $scope.errorMssg1 = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                fileUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename
                        }
                        fileIdsArray.push(fileDetails);
                        $scope.scheme.file.push(fileDetails);
                        $scope.uploadFileName.push(fileDetails);
                        filedetails.push(response.data);
                        fileUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.filesUplaod = true;
                $scope.disable = false;
                $scope.errorMssg1 = false;
            }
        });
    };

    var formIdsArray=[];
    var formUploadStatus=true;
    $scope.uploadFromName = [];
    $scope.uploadForms = function (files) {
        formIdsArray=[];
        var fileCount = 0;
       angular.forEach(files, function(file) {
            $scope.disable = true;
            $scope.errorMssg = true;
            fileCount++;
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        var fileDetails = {
                            'id': response.data._id,
                            'name': response.data.filename
                        };
                        formIdsArray.push(fileDetails);
                        $scope.scheme.forms.push(fileDetails);
                        $scope.uploadFromName.push(fileDetails);
                        formUploadStatus = true;
                        file.result = response.data;
                    });

                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }

            if (fileCount == files.length) {
                $scope.formsUplaod = true;
                $scope.disable = false;
                $scope.errorMssg = false;
            }
        });
    };

    $scope.reset=function () {
        $scope.scheme={}
    }
    $scope.clickToBack = function(){
        $location.url("/schemes");
    }
    $scope.getEmployeeType=function () {
        $http({
            method: 'GET',
            url: 'api/Employees?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            //console.log('Users Response :' + JSON.stringify(response));
            $scope.employeeList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getEmployeeType();
    $scope.bineficiaries=[];
    $scope.bineficiariesList=[];
    $scope.getBeneficiaries = function() {
        $http({
            method: 'GET',
            url: 'api/beneficiariesLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response) {
            $scope.beneficiariesLists = response;
            if($scope.beneficiariesLists!=undefined && $scope.beneficiariesLists !=null && $scope.beneficiariesLists.length>0 ){
                for(var i=0;i<$scope.beneficiariesLists.length;i++ ){
                    var data={
                        label:$scope.beneficiariesLists[i].name,
                        id: $scope.beneficiariesLists[i].name
                    }
                    $scope.bineficiariesList.push(data);
                }
            }
        }).error(function (response) {
            console.log('Error getBeneficiaries :' + JSON.stringify(response));
        });
    }
    $scope.getBeneficiaries();

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/Departments?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.departmentList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getDepartments();

    $scope.getMinistries = function() {
        $http({
            method: 'GET',
            url: 'api/ministeriesLists?filter=%7B%22where%22%3A%7B%22status%22%3A%20%22Active%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response) {
            $scope.ministriesLists = response;
        }).error(function (response) {
            console.log('Error getMinistries :' + JSON.stringify(response));
        });
    }
    $scope.getMinistries();


    $scope.getDesignation=function () {
        $http({
            method: 'GET',
            url: 'api/Designations',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.designationList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $scope.getDesignation();

    $scope.pack=$routeParams.packId;
    console.log('edit scheme details :' + JSON.stringify($scope.pack));

    $http({
        method: 'GET',
        url: 'api/Schemes/'+$scope.pack,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        $scope.scheme = response;
        var benfiList=response.bineficiaries;
        $scope.getEmpData($scope.scheme.contactOne.employeeId);
        $scope.getEmpData1($scope.scheme.contactTwo.employeeId);
        $scope.getEmpData2($scope.scheme.contactThree.employeeId);

        if(benfiList!=undefined && benfiList!=null && benfiList.length>0){
            for(var i=0;i<benfiList.length;i++){
                if(benfiList[i]!=null){
                    var dataId={
                        id: benfiList[i].label
                    };
                    $scope.bineficiaries.push(dataId);
                }
            }
        }

    }).error(function (response) {
        console.log('Error Response :' + JSON.stringify(response));
    });

    $scope.getEmpData = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('EMP data 1...... :    ' + JSON.stringify(response));
            $scope.selectedEMP = response[0];
            $scope.showEMPDetails = false;
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.getEmpData1 = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectedEMP1 = response[0];
            $scope.showEMPDetails1 = false;
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

     $scope.getEmpData2 = function(emp) {
        $scope.empID = emp;
        $http({
            method: 'GET',
            url: 'api/Employees/?filter={"where":{"employeeId":"' + $scope.empID  + '"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.selectedEMP2 = response[0];
            $scope.showEMPDetails2 = false;
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.editScheme=function(){
        if(fileUploadStatus && formUploadStatus) {
            var beneficals = [];
            if ($scope.bineficiaries != null && $scope.bineficiaries.length > 0) {
                for (var i = 0; i < $scope.bineficiaries.length; i++) {
                    beneficals.push($scope.bineficiaries[i].id)
                }
            }
            var schemeDetails = $scope.scheme;
            schemeDetails.bineficiaries = beneficals;
                $http({
                "method": "PUT",
                "url": 'api/Schemes/' + $scope.scheme.id,
                "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                "data": $scope.scheme
            }).success(function (response, data) {
                $scope.scheme = response;
                $('#editSchemeSuccess').modal('show');
            }).error(function (response, data) {
                console.log("failure");
            })
        }else{
            $scope.editScheme();
        }
    }
    $scope.editSchemeSuccessModal = function() {
        $('#editSchemeSuccess').modal('hide');
        location.href = '#/schemes';
        $window.location.reload();
    }

    $scope.removeFile = function(index, fileId, type){

        if(type == 'file') {
            console.log('File');
            $scope.scheme.file.splice(index, 1);
            for(var i=0; i<$scope.uploadFileName.length; i++){
                if($scope.uploadFileName[i].id == fileId){
                    $scope.uploadFileName.splice(i,1);
                }
            }
            if($scope.uploadFileName.length == 0) {
                $scope.filesUplaod = false;
            }
        }else{
            console.log('Form');
            $scope.scheme.forms.splice(index, 1);
            for(var i=0; i<$scope.uploadFromName.length; i++){
                if($scope.uploadFromName[i].id == fileId){
                    $scope.uploadFromName.splice(i,1);
                }
            }
            if($scope.uploadFromName.length == 0) {
                $scope.formsUplaod = false;
            }
        }
    };

}]);

app.controller('viewSchemeController',  ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', function(schemeManagement, $http, $scope, $window, $location, $rootScope, $routeParams) {
    console.log("viewSchemeController");

    $(document).ready(function () {
             $('html,body').scrollTop(0);
             $(".sidebar-menu li ul > li").removeClass('active1');
             $('[data-toggle="tooltip"]').tooltip();
             $(".sidebar-menu #schemesLi").addClass("active");
             $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
             $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
             $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes").addClass("active1").siblings('.active1').removeClass('active1');
             $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.schemes .collapse").addClass("in");
             $(".sidebar-menu li .collapse").removeClass("in");
         });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeManagement'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.loadingImage=true;
        $scope.pack=$routeParams.packId;
    $scope.uploadURL=uploadFileURL;
    $http({
        method: 'GET',
        url: 'api/Schemes/'+$scope.pack,
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        $scope.loadingImage=false;
        $scope.scheme = response;
    }).error(function (response) {
        console.log('Error Response :' + JSON.stringify(response));
    });
    $scope.clickToBack = function(){
        $location.url("/schemes");
    }

}]);

app.controller('editRequestController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, $routeParams, DTOptionsBuilder, DTColumnBuilder) {
    console.log("editRequestController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #schemes li.requests").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #schemesLi.treeview").addClass("active");
        $(".sidebar-menu #schemes li.requests .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeRequests'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.clickToBack = function(){
        $location.url("/requests");
    }
    $scope.requestDetails=$routeParams.requstId;
    $scope.uploadFile=uploadFileURL;
    $http({
        "method": "GET",
        "url": 'api/Requests/'+$scope.requestDetails,
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        $scope.loadingImage=false;
        $scope.requestLists = response;
    }).error(function (response, data) {
        console.log("failure");
    })

    $scope.acceptStatus = function (request) {
        var updateDetails = {
            'email':request.email,
            "acceptStatus": "Yes",
            "requestStatus": "Approval",
            "comment": request.comment
        };
        $scope.approvalScheme = true;
        $http({
            "method": "PUT",
            "url": 'api/Requests/'+ request.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            $scope.requestLists = response;
            $scope.loadingImage=false;
            location.href = '#/requests';
            $window.localStorage.setItem('requestFlag',true);
        }).error(function (response, data) {
            console.log("failure");
        })
    }
    $scope.rejectStatus = function (request) {
        var updateDetails = {
            'email':request.email,
            "acceptStatus": "No",
            "requestStatus": "Rejected",
            "comment": request.comment
        };
        $http({
            "method": "PUT",
            "url": 'api/Requests/'+ request.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": updateDetails
        }).success(function (response, data) {
            $scope.requestLists = response;
            $scope.loadingImage=false;
            location.href = '#/requests';
            $window.localStorage.setItem('requestFlag',true);
        }).error(function (response, data) {
            console.log("failure");
        })
    }

}]);

app.controller('viewRequestController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, $routeParams,Excel,$timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("viewRequestController");

    $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #schemesLi").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro").addClass("active");
         $(".sidebar-menu #schemesLi.treeview #schemeMgmtPro.active #schemeMgmtSetup").addClass("menu-open");
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.requestsReport").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #scheme #schemeMgmtPro.active #schemeMgmtSetup li.requestsReport .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeRequests'}, function (response) {
//    alert(JSON.stringify(response));
       if(!response){
        window.location.href = "#/noAccessPage";
       }
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.loadingImage=true;
    $scope.clickToBack = function(){
        $scope.viewRequestDiv=true;
        $scope.viewRequestDivBack=true;
        $scope.requestDivBack=false;
        $scope.requestDiv=false;
    }
    $scope.requestDetails=$routeParams.requstId;
    $scope.uploadFile=uploadFileURL;
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var employeeId=$scope.userInfo.employeeId;
    $http({
        "method": "GET",
        "url": 'api/Requests?filter=%7B%22where%22%3A%7B%22or%22%3A%5B%7B%20%22requestStatus%22%3A%20%22Rejected%22%7D%2C%7B%20%22requestStatus%22%3A%20%22Approval%22%7D%5D%7D%7D',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        $scope.loadingImage=false;
        $scope.requestLists = response;
    }).error(function (response, data) {
        console.log("failure");
    })
    $scope.viewRequestDivBack=true;
    $scope.viewRequestDiv=true;
    $scope.viewStatus=function (id) {
        $scope.loadingImage=false;
        if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
            for(var i=0;i<$scope.requestLists.length;i++){
                if(id==$scope.requestLists[i].id){
                    $scope.viewRequestDetails=angular.copy($scope.requestLists[i]) ;
                    $scope.viewRequestDiv=false;
                    $scope.viewRequestDivBack=false;
                    $scope.requestDivBack=true;
                    $scope.requestDiv=true;
                }
            }
        }
    }
    $scope.exportToExcel=function(tableId){
        if( $scope.requestLists!=null &&  $scope.requestLists.length>0){

            $("<tr>" +
                "<th>schemeName</th>" +
                "<th>requestId</th>" +
                "<th>Name</th>" +
                "<th>emailId</th>" +
                "<th>mobileNumber</th>" +
                "<th>createdTime</th>" +
                "<th>acceptLevel</th>" +
                "<th>maxlevel</th>" +
                "<th>finalStatus</th>" +
                "<th>requestStatus</th>" +
                "<th>acceptStatus</th>" +
                "<th>comment</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.requestLists.length;i++){
                var schemes=$scope.requestLists[i];

                $("<tr>" +
                    "<td>"+schemes.schemeName+"</td>" +
                    "<td>"+schemes.requestId+"</td>" +
                    "<td>"+schemes.name+"</td>" +
                    "<td>"+schemes.emailId+"</td>" +
                    "<td>"+schemes.mobileNumber+"</td>" +
                    "<td>"+schemes.createdTime+"</td>" +
                    "<td>"+schemes.acceptLevel+"</td>" +
                    "<td>"+schemes.maxlevel+"</td>" +
                    "<td>"+schemes.finalStatus+"</td>" +
                    "<td>"+schemes.requestStatus+"</td>" +
                    "<td>"+schemes.acceptStatus+"</td>" +
                    "<td>"+schemes.comment+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);


//*********** schemeFormsController**************************************
app.controller('schemeFormsController', function($http, $scope, $window, $location, $rootScope) {
    $scope.getScheme = function () {
        $http({
            "method": "GET",
            "url": 'api/SchemeForms',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $rootScope.SchemeData = response;
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getScheme();
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.scheme = {};
});
//*********** schemeFormsController**************************************

//*********** createSchemeFormsController*********************************

app.controller('createSchemeFormsController', function($http, $scope, Upload, $timeout,$window, $location, $rootScope) {
    var filedetails=[];
    var fileIdsArray=[];
    var fileUploadStatus=true;
    $scope.getModules = function() {
        $http({
            method: 'GET',
            url: 'api/Schemes',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.module = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    };
    $scope.getModules();
    $scope.getScheme = function () {
        var URL= 'http://localhost:3004/api';
        $http({
            "method": "GET",
            "url": 'api/SchemeForms',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $rootScope.SchemeData = response;

        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getScheme();
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.scheme = {};
    var formIdsArray=[];
    var formUploadStatus=true;
    $scope.docList = [];
    $scope.uploadDocuments = function (files) {
        formIdsArray=[];
        $scope.forms = files;
        angular.forEach(files, function(file) {
            if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                formUploadStatus = false;
                file.upload = Upload.upload({
                    url: 'api/Uploads/dhanbadDb/upload',
                    data: {file: file}
                });
                file.upload.then(function (response) {
                    $timeout(function () {
                        formIdsArray.push(response.data);
                        $scope.docList.push(response.data);
                        formUploadStatus = true;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }else{
                alert('Please Upload JPEG or PDF files only');
            }
        });
    };
    $scope.createSchemeForm = function() {
       if(fileUploadStatus && formUploadStatus){
            var schemeDetails=$scope.user;
            schemeDetails.file=fileIdsArray;
            schemeDetails.forms=$scope.docList;
            $http({
                method: 'POST',
                url: 'api/SchemeForms',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: schemeDetails
            }).success(function (response) {
                location.href = '#/schemeForms';
                $rootScope.SchemeData.push(response);
                $scope.user = {};
                $scope.docList = [];
                $('#myModal').modal('hide');
            }).error(function (response) {
                console.log('Error Response1 :' + JSON.stringify(response));
            });

        }else{
            $scope.createSchemeForm();
        }
    }
});
//*********** createSchemeFormsController*********************************
//*********** viewSchemeFormsController*********************************

app.controller('viewSchemeFormsController', function($http, $scope, $window, $location, $rootScope, $routeParams) {
    $scope.schemeDetails=$routeParams.formId;
       $http({
        "method": "GET",
        "url": 'api/SchemeForms/'+$scope.schemeDetails,
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        console.log("formId Schemes "+ JSON.stringify(response));
        $scope.schemeLists = response;
    }).error(function (response, data) {
        console.log(" schemeDetails failure");
    })
});


app.controller('beneficiariesListController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(schemeManagement, $http, $scope, $window, $location, $rootScope, $routeParams, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {


  $(document).ready(function () {
              $('html,body').scrollTop(0);
              $(".sidebar-menu li ul > li").removeClass('active1');
              $('[data-toggle="tooltip"]').tooltip();
              $(".sidebar-menu #schemesLi").addClass("active");
              $(".sidebar-menu #schemesLi.treeview #configLi").addClass("active");
              $(".sidebar-menu #schemesLi.treeview #configLi.active #masterDataschemeSetUp").addClass("menu-open");
              $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.beneficiariesList").addClass("active1").siblings('.active1').removeClass('active1');
              $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.beneficiariesList .collapse").addClass("in");
              $(".sidebar-menu li .collapse").removeClass("in");
          });

var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'beneficiaries'}, function (response) {
//        		alert(JSON.stringify(response));
        		   if(!response){
        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $scope.loadingImage=true;
    $scope.errorMessage=false;
    $scope.addBeneficiariesModel = function() {
        $("#addBeneficiaries").modal("show");
        $scope.beneficiaries = {};
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);
    $scope.addBeneficiaries=function () {
        if($scope.beneficiaries.name != null && $scope.beneficiaries.name != "" && $scope.beneficiaries.name != undefined) {
            if ($scope.beneficiaries.status != null && $scope.beneficiaries.status != "" && $scope.beneficiaries.status != undefined) {
                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                var beneficiaries=$scope.beneficiaries;
                beneficiaries['createdPerson']=loginPersonDetails.name;
                $http({
                    method: 'POST',
                    url: 'api/beneficiariesLists',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": beneficiaries
                }).success(function (response) {
                    $scope.loadingImage=false;
                    $("#addBeneficiaries").modal("hide");
                    $("#addBenifSuccess").modal("show");
                    setTimeout(function(){$('#addBenifSuccess').modal('hide')}, 3000);
                    $scope.getBeneficiaries();
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }
                });
            } else {
                $scope.errorShow = true;
                $scope.showError = 'Please Select Status';
                $timeout(function(){
                    $scope.errorShow=false;
                }, 3000);
            }
        } else {
            $scope.errorShow = true;
            $scope.showError = 'Please Enter Beneficiaries Name ';
            $timeout(function(){
                $scope.errorShow=false;
            }, 3000);
        }
    }
    $scope.getBeneficiaries = function() {
        $http({
            method: 'GET',
            url: 'api/beneficiariesLists',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.beneficiariesLists = response;
        }).error(function (response) {
        });
    }
    $scope.getBeneficiaries();
    $scope.editBeneficiaries = function(beneficiaries) {
        $("#editBeneficiaries").modal("show");
        $scope.editBeneficiariesLists = angular.copy(beneficiaries);
    }
    $scope.editBeneficiariesButton=function () {
        var editBeneficiariesDetails= $scope.editBeneficiariesLists;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editBeneficiariesDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/beneficiariesLists/'+$scope.editBeneficiariesLists.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editBeneficiariesDetails
        }).success(function (response, data) {
            $scope.loadingImage=false;
            $("#editBeneficiaries").modal("hide");
            $("#editBenifSuccess").modal("show");
            setTimeout(function(){$('#editBenifSuccess').modal('hide')}, 3000);
            $scope.getBeneficiaries();
        }).error(function (response, data) {
            console.log("failure");
        })
    }


    $scope.exportToExcel=function(tableId) {
        if( $scope.beneficiariesLists!=null &&  $scope.beneficiariesLists.length>0){

            $("<tr>" +
                "<th>Beneficiaries Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.beneficiariesLists.length;i++){
                var beneficiaries=$scope.beneficiariesLists[i];
                $("<tr>" +
                    "<td>"+beneficiaries.name+"</td>" +
                    "<td>"+beneficiaries.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Request Lists Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);

//app.controller('ministriesListController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', '$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(schemeManagement, $http, $scope, $timeout, $window, $location, $rootScope, $routeParams, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
app.controller('ministriesListController', ['schemeManagement', '$http', '$scope', '$timeout', '$window', '$location', '$rootScope', '$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder' ,function(schemeManagement, $http, $scope, $timeout, $window, $location, $rootScope, $routeParams, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("ministriesListController");


    $(document).ready(function () {
                $('html,body').scrollTop(0);
                $(".sidebar-menu li ul > li").removeClass('active1');
                $('[data-toggle="tooltip"]').tooltip();
                $(".sidebar-menu #schemesLi").addClass("active");
                $(".sidebar-menu #schemesLi.treeview #configLi").addClass("active");
                $(".sidebar-menu #schemesLi.treeview #configLi.active #masterDataschemeSetUp").addClass("menu-open");
                $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.ministriesList").addClass("active1").siblings('.active1').removeClass('active1');
                $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.ministriesList .collapse").addClass("in");
                $(".sidebar-menu li .collapse").removeClass("in");
            });

    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'ministries'}, function (response) {
//        		alert(JSON.stringify(response));
        		   if(!response){
        			window.location.href = "#/noAccessPage";
        		   }
        		});

    $scope.loadingImage=true;
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.errorMessage=false;
    $scope.addministryModal = function(){
        $scope.ministries = {}
        $("#addMinistries").modal("show");
    }
    var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    $scope.addMinistries=function () {
        var ministries=$scope.ministries;
        ministries['createdPerson']=loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/ministeriesLists',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data": ministries
        }).success(function (response) {
            $scope.loadingImage=false;
            $("#addMinistries").modal("hide");
            $('#addMinistriesSuccess').modal('show');
            setTimeout(function(){$('#addMinistriesSuccess').modal('hide')}, 3000);
            $scope.getMinistries();
        }).error(function (response) {
            if(response.error.details.messages.name) {
                $scope.errorMessageData=response.error.details.messages.name[0];
                $scope.errorMessage=true;
                $timeout(function(){
                    $scope.errorMessage=false;
                }, 3000);
            }

            console.log('Error addMinistries :' + JSON.stringify(response));
        });
    }
    $scope.getMinistries = function() {
        $http({
            method: 'GET',
            url: 'api/ministeriesLists',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
        }).success(function (response) {
            $scope.loadingImage=false;
            $scope.ministriesLists = response;
        }).error(function (response) {
        });
    }
    $scope.getMinistries();

    $scope.editMinistries = function(ministries) {
        $("#editMinistries").modal("show");
        $scope.editMinistriesLists = angular.copy(ministries);
        $scope.editMinistriesListsOld = angular.copy(ministries);
    }

    $scope.editMinistriesButton=function () {
        var editMinistriesDetails= $scope.editMinistriesLists;
        var editMinistriesDetailsOld= $scope.editMinistriesListsOld;
        $scope.editMinistriesDetailsOld1 = editMinistriesDetailsOld;
        editMinistriesDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/ministeriesLists/'+$scope.editMinistriesLists.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editMinistriesDetails
        }).success(function (response, data) {
            $scope.loadingImage=false;
            $("#editMinistries").modal("hide");
            $('#editMinistriesSuccess').modal('show');
            setTimeout(function(){$('#editMinistriesSuccess').modal('hide')}, 3000);
            $scope.getMinistries();
        }).error(function (response, data) {
            //console.log("failure");
        })
    }
    $scope.exportToExcel=function(tableId){
        if( $scope.ministriesLists!=null &&  $scope.ministriesLists.length>0){

            $("<tr>" +
                "<th>Ministry Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.ministriesLists.length;i++){
                var ministries=$scope.ministriesLists[i];

                $("<tr>" +
                    "<td>"+ministries.name+"</td>" +
                    "<td>"+ministries.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Ministry List Details ');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }
}]);

//********************* Admin loginController****************************************
app.controller('loginController', function($scope, $http, $rootScope, $window, $location) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $('[data-toggle="tooltip"]').tooltip();
    });
    console.log('Login Controller');
    $scope.user = {};
    $scope.showLoginError = false;
    $scope.loginError = '';
    $scope.loginSubmit = function(){
        $scope.showLoginError = false;
        $scope.loginError = '';
        if(checkMailFormat($scope.user.email)) {
            $http({
                method: 'POST',
                url: 'api/Employees/login',
                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                data: $scope.user
            }).success(function (response) {
                $window.localStorage.setItem('accessToken', response.id);
                $window.localStorage.setItem('tokenId', response.id);
                 $rootScope.userLogin = true;
                $scope.user = {};
                if(response.userInfo) {
                    $rootScope.userName = response.userInfo.name;
                    $window.localStorage.setItem('userName', response.userInfo.name);
                    $window.localStorage.setItem('userDetails', JSON.stringify(response.userInfo));
                    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
                    $window.localStorage.setItem('afterLogin', 'true');
                    $scope.roleList= response.roleObject;
                    $window.localStorage.setItem('roleList', JSON.stringify( $scope.roleList));
                    window.location.href = "/admin/#/home";
                    var employeeId=response.userInfo.employeeId;
                    var details={
                        'employeeId':employeeId
                    }
                }
            }).error(function (response) {
                $scope.showLoginError = true;
                $scope.loginError = 'Invalid Email or Password';
            });
        }else{
            $scope.showLoginError = true;
            $scope.loginError = 'Please enter a valid Email';
        }
    };
    $scope.forgotPasswordData = function (email) {
        $rootScope.email = '';
        $scope.successMessage='';
        $scope.message='';
        var email1 = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        $rootScope.email = document.getElementById('email').value;
        var searchData=$rootScope.email;
        if ($rootScope.email == undefined || $rootScope.email == '' || $rootScope.email == null) {
            $rootScope.notRegisterd = "Enter your Email";
        } else if (email1.test($rootScope.email)) {
            $http({
                method: "GET",
                url: 'api/Employees/forgotPassword?searchData={"emailId":"' + $rootScope.email+'"}',
                headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
            }).success(function (response) {
              if(response.data.message=="Please Register With US")
                {
                    $scope.message="Invalid Email"
                }
                else {
                    $scope.successMessage="Your password is send to your Email"
                }
            }).error(function (response) {
            });
        } else {
            $rootScope.notRegisterd = "Please Enter Valid Email";
        }
    }
});
//********************* Admin loginController*****************************************

//*********************index controller*******************
app.controller('indexController', function($scope, $rootScope, $window, $location, $http) {
    $(document).ready(function () {
        $('html,body').scrollTop(0);
        var a = moment().format('MMMM Do YYYY, h:mm:ss a');
        $scope.currentDate = a;

    });

            $scope.loading = true;
    $scope.clickMe = function() {
    console.log("..................");
                $scope.loading = false;
          }
          $scope.clickMe();

    $rootScope.userLogin = false;
    $rootScope.userName = $window.localStorage.getItem('userName');
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
    var documentURL=document.URL;
    var res = documentURL.split("grievanceModulelogin");
    if($scope.userInfo){
        $rootScope.userLogin = true;
    }else{
        if(res.length>1){

        }else{
            location.href="http://"+$location.host()+":3003/admin/login.html#/";
        }
    }
    var roleLogin=   JSON.parse($window.localStorage.getItem('roleList'));
    if(roleLogin){
        $rootScope.roleLogin=roleLogin;
    }
    $scope.getNotification=function (employeeId) {
        $http({
            method: 'GET',
            url: 'api/notifications/getNotificationDetails?employeeId='+employeeId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {

            $rootScope.notificationData=response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }
    $rootScope.updateNotificationStatus=function(notificaion){
        var notificationData={
            'notificationId':notificaion.id,
            'employeeId':$scope.userInfo.employeeId
        }
        $http({
            method: 'POST',
            url: 'api/notifications/updateNotification',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data:notificationData
        }).success(function (response) {
            window.location.href = '#/'+notificaion.urlData;
            $window.location.reload();
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }


    if($scope.userInfo){
        $scope.getNotification($scope.userInfo.employeeId);
    }
    $rootScope.logout = function () {
        $window.localStorage.clear();
        location.href="http://"+$location.host()+":3003/admin/login.html#/";

        //window.location.href = "/admin/login.html";
        $window.location.reload();
    };
});
//***********************index controller******************************
//***********************userProfileController******************************
app.controller('userProfileController', function($http, $scope, $timeout, $window, $location, $rootScope) {
    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
     $scope.getEmployee = function () {
        $http({
            "method": "GET",
            "url": 'api/Employees',
            "headers": {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $rootScope.employeeData = response;
        }).error(function (response, data) {
            console.log("failure");
        });
    };
    $scope.getEmployee();
    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };
    $scope.employee = {};
    $scope.userNameChange = function (name,id) {
        $scope.accessToken = $window.localStorage.getItem('accessToken');
        $http({
            method: "PUT",
            url: 'api/Employees/'+ id, /*+ '?access_token=' + $scope.accessToken*/
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: {
                "name": name
            }
        }).success(function (response) {
            $window.localStorage.setItem('userDetails', JSON.stringify(response));
            $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
            $("#changeNameField").modal('hide');
            $window.location.reload();
        }).error(function (response) {
        });

    }
    $scope.changeMobileNumber = function(userInfo) {
        $("#editPhone").modal("show");
        $scope.editMobileNumber=angular.copy(userInfo);
    }
    var phoneno = /^[0-9]{1}[0-9]{9}$/;
    $scope.editMobileSubmit = function (mobile,id) {
        if (phoneno.test($scope.editMobileNumber.mobile) || $scope.editMobileNumber.mobile<10) {
                $scope.accessToken = $window.localStorage.getItem('accessToken');
                $http({
                    method: "PUT",
                    url: 'api/Employees/'+id,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data: {
                        "mobile": mobile
                    }
                }).success(function (response) {
                    $window.localStorage.setItem('userDetails', JSON.stringify(response));
                    $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
                    $("#editPhone").modal('hide');
                    $("#mobileNumberSuccess").modal('show');
                    setTimeout(function(){$('#mobileNumberSuccess').modal('hide')}, 3000);
                    //$window.location.reload();
    
                }).error(function (response) {
                });

         } else {
            $scope.errorMessage = true;
            $timeout(function(){
                $scope.errorMessage=false;
            }, 3000);
        }
    }

    $scope.editEmailSubmit = function (email,id) {
        $scope.accessToken = $window.localStorage.getItem('accessToken');
        $http({
            method: "PUT",
            url: 'api/Employees/'+id,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data: {
                "email": email
            }
        }).success(function (response) {
            $window.localStorage.setItem('userDetails', JSON.stringify(response));
            $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
            $window.location.reload();
            $("#editEmailField").modal('hide');
        }).error(function (response) {
        });
    }
    /*change password start*/
    $scope.changePasswordModal = function () {
        $('#changePassword').modal('show');
        /*$scope.user = {};*/
        document.getElementById("password").value = '';
        document.getElementById("pswd").value = '';
        document.getElementById("pswd1").value = '';
    }
    $scope.user = {};
    $scope.changePasswordSubmit = function () {

        $scope.tokenId = $window.localStorage.getItem("tokenId");
        //alert("tokenId" +$scope.tokenId);

        var currentpassword = document.getElementById("password").value;
        //alert("current password:::" +currentpassword);
        var password = document.getElementById("pswd").value;
        //alert(" password:::" +password);
        var cpasswor = document.getElementById("pswd1").value;
        //alert("confirm password:::" +cpasswor);
        $scope.passwordError1 = "";

        if (currentpassword != '' && currentpassword != undefined && currentpassword != null
            && password != '' && password != undefined && password != null
            && cpasswor != '' && cpasswor != undefined && cpasswor != null) {
            if (password.length >= 6) {
                if (password == cpasswor) {
                    $http({
                        "method": "POST",
                        "url": 'api/Employees/login',
                        "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                        "data": {
                            "email": $scope.userInfo.email,
                            "password": $scope.user.password
                        }
                    }).success(function (response) {

                        console.log("response" +JSON.stringify(response));
                        $scope.tokenId = response.id;
                        $scope.userId = response.userId;
                        $window.localStorage.setItem('accessToken', $scope.tokenId);

                        $http({
                            method: "PUT",
                            url:   "api/Employees/" + $scope.userInfo.id ,
                            headers: {'Accept': 'application/json'},
                            data: {"password": $scope.user.newPassword}
                        }).success(function (response, data) {
                            console.log("Password Response:" + JSON.stringify(response));
                            //document.getElementById('myform').reset();
                            $('#changePassword').modal('hide');
                            $('#changePasswordSuccess').modal('show');
                            setTimeout(function(){$('#changePasswordSuccess').modal('hide')}, 2000);
                            //$scope.passwordError = "Password Updated Successfully";
                            //console.log("Password Updated Successfully");

                        }).error(function (response) {
                            //console.log("Error:" + JSON.stringify(response));
                            //$scope.passwordError = true;
                            $scope.passwordError1 = "Something Went Wrong. Please Try Again Later";
                            //console.log("Something Went Wrong. Please Try Again Later");

                            $scope.errorMessagePSW = true;
                            $timeout(function(){
                                $scope.errorMessagePSW=false;
                            }, 3000);

                        })
                    }).error(function (data) {
                        $scope.passwordError1 = "Please Enter Correct Password";
                        //console.log("Please Enter Correct Password");

                        $scope.errorMessagePSW = true;
                        $timeout(function(){
                            $scope.errorMessagePSW=false;
                        }, 3000);

                    })
                } else {
                    $scope.passwordError1 = "New password & confirm password does not match";
                    //console.log("Password Confirmation Unsuccessful.");
                    $scope.errorMessagePSW = true;
                    $timeout(function(){
                        $scope.errorMessagePSW=false;
                    }, 3000);
                }
            }
            else {
                console.log("Enter Minimum 6 Characters");

            }
        } else {
            console.log("All Fields Are Mandatory.");

        }
    }


    /*change password end*/






});
//***********************userProfileController******************************

function checkMailFormat(checkData){
    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(checkData.match(mailFormat)){
        return true;
    }else{
        ////////alert('Entered');
        return false;
    }
}

function checkAlphaNumeric(checkData){
    var alphaNumeric =/^[a-zA-Z0-9]+$/;
    if(checkData.match(alphaNumeric)){
        return true;
    }else{
        return false;
    }

}

function checkPhoneNumber(checkData){
    var phoneNumber =/^[7-9]{1}[0-9]{9}$/;
    if(checkData.match(phoneNumber)){
        return true;
    }
    else{
        return false;
    }
}

//app.controller('departmentController', function($http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
app.controller('departmentController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("departmentController");

    $(document).ready(function () {
            $('html,body').scrollTop(0);
            $(".sidebar-menu li ul > li").removeClass('active1');
            $('[data-toggle="tooltip"]').tooltip();
            $(".sidebar-menu #schemesLi").addClass("active");
            $(".sidebar-menu #schemesLi.treeview #configLi").addClass("active");
            $(".sidebar-menu #schemesLi.treeview #configLi.active #masterDataschemeSetUp").addClass("menu-open");
            $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.schemeDepartment").addClass("active1").siblings('.active1').removeClass('active1');
            $(".sidebar-menu #scheme #configLi.active #masterDataschemeSetUp li.schemeDepartment .collapse").addClass("in");
            $(".sidebar-menu li .collapse").removeClass("in");
        });

 var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
    		schemeManagement.getFormAccess({'employeeId':loginPersonDetails.employeeId,'formName':'schemeDepartment'}, function (response) {
//    		alert(JSON.stringify(response));
    		   if(!response){
    			window.location.href = "#/noAccessPage";
    		   }
    		});

    /*function WithBootstrapOptionsCtrl(DTOptionsBuilder, DTColumnBuilder) {
        var vm = this;
        vm.dtOptions = DTOptionsBuilder
            // .fromSource('data.json')
            // .withDOM('&lt;\'row\'&lt;\'col-xs-6\'l&gt;&lt;\'col-xs-6\'f&gt;r&gt;t&lt;\'row\'&lt;\'col-xs-6\'i&gt;&lt;\'col-xs-6\'p&gt;&gt;')
            // Add Bootstrap compatibility
            .withBootstrap()
            .withBootstrapOptions({
                TableTools: {
                    classes: {
                        container: 'btn-group',
                        buttons: {
                            normal: 'btn btn-danger'
                        }
                    }
                },
                ColVis: {
                    classes: {
                        masterButton: 'btn btn-primary'
                    }
                },
                pagination: {
                    classes: {
                        ul: 'pagination pagination-sm'
                    }
                }
            })

            // Add ColVis compatibility
            .withColVis()

            // Add Table tools compatibility
            .withTableTools('vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf')
            .withTableToolsButtons([
                'copy',
                'print', {
                    'sExtends': 'collection',
                    'sButtonText': 'Save',
                    'aButtons': ['csv', 'xls', 'pdf']
                }
            ]);
        vm.dtColumns = [
            DTColumnBuilder.newColumn('name').withTitle('Department Name').withClass('text-danger'),
            DTColumnBuilder.newColumn('status').withTitle('Status'),
            DTColumnBuilder.newColumn('action').withTitle('Action')
        ];
    }*/

    $scope.loadingImage=true;
    /*$scope.vm = {};*/

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    /*$scope.dtColumns = [
        DTColumnBuilder.newColumn('name').withHtml("<span lang=en>").withTitle('Department Name').withClass('text-danger'),
        DTColumnBuilder.newColumn('status').withTitle('Status'),
        DTColumnBuilder.newColumn('action').withTitle('Action')
    ];*/


    $scope.editDepartment = function(department) {
        $("#editDepartment").modal("show");
        /*$scope.editCharterArea = true;
         $scope.createCharterArea = false;*/
        $scope.editDepartmentData=angular.copy(department);
    }

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/Departments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;

        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.addDepartmentModal = function() {
        $("#addDepartment").modal("show");
        $scope.department = {};
        $scope.errorMessageData = '';
        $scope.errorMessage=false;
    }
    $scope.addDepartment=function () {
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        var department=$scope.department;
        department['createdPerson']=loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/Departments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":department
        }).success(function (response) {
            console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $("#addDepartment").modal("hide");
            $("#addDeptSuccess").modal("show");
            setTimeout(function(){$('#addDeptSuccess').modal('hide')}, 3000);
            $scope.getDepartments();
        }).error(function (response) {
            if(response.error.details.messages.name) {
                $scope.errorMessageData=response.error.details.messages.name[0];
                $scope.errorMessage=true;
            }
            $timeout(function(){
                $scope.errorMessage=false;
            }, 3000);

        });
    }

    $scope.editDepartmentButton=function () {
        var editCharterDetails= $scope.editDepartmentData;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editCharterDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/Departments/'+$scope.editDepartmentData.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editCharterDetails
        }).success(function (response, data) {
            //console.log("filter Schemes "+ JSON.stringify(response));
            $scope.loadingImage=false;
            $("#editDepartment").modal("hide");
            $("#editDeptSuccess").modal("show");
            setTimeout(function(){$('#editDeptSuccess').modal('hide')}, 3000);
            $scope.getDepartments();
        }).error(function (response, data) {
            if(response.error.details.messages.name) {
                $scope.errorMessageUpdateData=response.error.details.messages.name[0];
                $scope.errorUpdateMessage=true;
            }
            $timeout(function(){
                $scope.errorUpdateMessage=false;
            }, 3000);
            console.log("failure");
        })
    }

    $scope.getDepartments();

    $scope.exportToExcel=function(tableId){
        if( $scope.departmentList!=null &&  $scope.departmentList.length>0){

            $("<tr>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.departmentList.length;i++){
                var department=$scope.departmentList[i];

                $("<tr>" +
                    "<td>"+department.name+"</td>" +
                    "<td>"+department.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Department Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

}]);
app.controller('schemeModuleController', ['schemeManagement', '$http', '$scope', '$window', '$location', '$rootScope','$routeParams', 'Excel', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', function(schemeManagement, $http, $scope, $window, $location, $rootScope,$routeParams, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("schemeModuleController");

    var data={
        'employeeId':$routeParams.employeeId
    }
    $http({
        "method": "GET",
        "url": 'api/Schemes/getEmployeeData?searchData=%7B%22employeeId%22%20%3A%20%22'+$routeParams.employeeId+'%22%7D',
        "headers": {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response, data) {
        $window.localStorage.setItem('accessToken', response.data.id);
        $window.localStorage.setItem('tokenId', response.data.id);
        $rootScope.userLogin = true;
        $scope.user = {};
        if(response.data.userInfo) {
            $rootScope.userName = response.data.userInfo.name;
            $window.localStorage.setItem('userName', response.data.userInfo.name);
            //$window.localStorage.setItem('employeeId', response.userInfo.employeeId);
            $window.localStorage.setItem('userDetails', JSON.stringify(response.data.userInfo));
            $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
            $window.localStorage.setItem('afterLogin', 'true');
            $scope.roleList= response.data.roleObject;
            $window.localStorage.setItem('roleList', JSON.stringify( response.data.roleObject));
            //window.location.href ="selectModule.html";
            //var employeeId=response.data.userInfo.employeeId;
            location.href="http://"+$location.host()+":3015/admin/"

        }

    }).error(function (response, data) {
        console.log("failure");
    })

}]);
