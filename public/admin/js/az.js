app.controller('createAssetOthersController', function($scope, $rootScope, $window, $parse, $location, $http, Excel, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     console.log("createAssetOthersController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetOthers").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetOthers .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

      //$scope.uploadFile='api/Uploads/dhanbadDb/download/';
      $scope.uploadFile=uploadFileURL;

      $(document).ready(function () {
          $('html,body').scrollTop(0);
          $(".navbar-nav li.landAndAsset").addClass("active").siblings('.active').removeClass('active');
          $(".navbar-nav li.landAndAsset li.assetType").addClass("active1").siblings('.active1').removeClass('active1');
          $(".navbar-nav li.schemes .collapse").removeClass("in");
          $(".navbar-nav li.landAndAsset .collapse").addClass("in");
          $(".navbar-nav li.configuration .collapse").removeClass("in");
          $(".navbar-nav li.administration .collapse").removeClass("in");

          $(".navbar-nav > li.projectWorks").addClass("active").siblings('.active').removeClass('active');
          $(".navbar-nav li.projectWorks li.masterData li.departmentList").addClass("active1").siblings('.active1').removeClass('active1');
          $(".navbar-nav li.projectWorks li.masterData .nav-list").addClass("active2").siblings('.active2').removeClass('active2');
          $(".navbar-nav li.projectWorks > .collapse").addClass("in");
          $(".navbar-nav li.masterData .collapse").addClass("in");
      });

      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.landData!=null &&  $scope.landData.length>0){
              $("<tr>" +
                  "<th>Asset Id</th>" +
                  "<th>Asset Type</th>" +
                  "<th>Asset Description</th>" +
                  "<th>Asset Name</th>" +
                  "<th>Asset Renewable Availability</th>" +
                  "<th>Asset Mode</th>" +
                  "<th>Contact Person Department</th>" +
                  "<th>Contact Person</th>" +
                  "<th>Phone Number</th>" +
                  "<th>Leased Date</th>" +
                  "<th>Terms In Months</th>" +
                  "<th>Village</th>" +
                  "<th>Status</th>" +
                  "<th>Duty Type</th>" +
                  "<th>Duty</th>" +
                  "<th>Manufacturing Serial Number</th>" +
                  "<th>Manufacturing Code</th>" +
                  "<th>Manufacturing Name</th>" +
                  "<th>Supplier Number</th>" +
                  "<th>Supplier Code</th>" +
                  "<th>Supplier Name</th>" +
                  "<th>Purchase Date</th>" +
                  "<th>Warrenty Period</th>" +
                  "<th>Expiry Date</th>" +
                  "<th>Maintenance Contact Name</th>" +
                  "<th>Original Life</th>" +
                  "<th>Expected Date Of Disposal</th>" +
                  "<th>Original Remaining Life</th>" +
                  "<th>Revised Remaining Life</th>" +
                  "<th>Replacement Due Date</th>" +
                  "<th>Asset Last Reviewed Date</th>" +
                  "<th>Impairment Life</th>" +
                  "<th>Cost Center</th>" +
                  "<th>Historic Cost Availability</th>" +
                  "<th>Historic Cost</th>" +
                  "<th>Amortization Method</th>" +
                  "<th>Amortization Rate</th>" +
                  "<th>Amortization Amount</th>" +
                  "<th>Book Value</th>" +
                  "<th>Return Value</th>" +
                  "<th>Scrap Value</th>" +
                  "<th>Control Account</th>" +
                  "<th>Adjustment Account</th>" +
                  "<th>Shrinkage Account</th>" +
                  "<th>Price Variance</th>" +
                  "<th>Currency Variance</th>" +
                  "<th>Purchase Price Variance</th>" +
                  "<th>Receipt Variance</th>" +
                  "<th>Criticality Asset</th>" +
                  "<th>Since Date</th>" +
                  "<th>Acquisition Status</th>" +
                  "<th>Ownership Transfer Status</th>" +
                  "<th>Request Transfer Name</th>" +
                  "<th>Required Date</th>" +
                  "<th>Legal Formality Status</th>" +
                  "<th>Asset Spares Name</th>" +
                  "<th>Asset Spares Comment</th>" +
                  "<th>Acquisition Comments</th>" +
                  "<th>Created Person</th>" +
                  "<th>Employee ID</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.landData.length;i++){
                  var land=$scope.landData[i];
                  $("<tr>" +
                      "<td>"+land.assetNumber+"</td>" +
                      "<td>"+land.assetType+"</td>" +
                      "<td>"+land.assetDescription+"</td>" +
                      "<td>"+land.assetName+"</td>" +
                      "<td>"+land.assetRenewableAvailability+"</td>" +
                      "<td>"+land.assetMode+"</td>" +
                      "<td>"+land.contactPersonDepartment+"</td>" +
                      "<td>"+land.contactPerson+"</td>" +
                      "<td>"+land.phoneNumber+"</td>" +
                      "<td>"+land.leasedDate+"</td>" +
                      "<td>"+land.termsInMonths+"</td>" +
                      "<td>"+land.village+"</td>" +
                      "<td>"+land.status+"</td>" +
                      "<td>"+land.dutyType+"</td>" +
                      "<td>"+land.duty+"</td>" +
                      "<td>"+land.manufacturingSNo+"</td>" +
                      "<td>"+land.manufacturingCode+"</td>" +
                      "<td>"+land.manufacturingName+"</td>" +
                      "<td>"+land.supplierNo+"</td>" +
                      "<td>"+land.supplierCode+"</td>" +
                      "<td>"+land.supplierName+"</td>" +
                      "<td>"+land.purchaseDate+"</td>" +
                      "<td>"+land.warrentyPeriod+"</td>" +
                      "<td>"+land.expiryDate+"</td>" +
                      "<td>"+land.maintenanceContactDepartment+"</td>" +
                      "<td>"+land.maintenanceContactName+"</td>" +
                      "<td>"+land.originalLife+"</td>" +
                      "<td>"+land.expectedDateOfDisposal+"</td>" +
                      "<td>"+land.originalRemainingLife+"</td>" +
                      "<td>"+land.revisedRemainingLife+"</td>" +
                      "<td>"+land.replacementDueDate+"</td>" +
                      "<td>"+land.assetLastReviewedDate+"</td>" +
                      "<td>"+land.impairmentLife+"</td>" +
                      "<td>"+land.costCenter+"</td>" +
                      "<td>"+land.historicCostAvailability+"</td>" +
                      "<td>"+land.historicCost+"</td>" +
                      "<td>"+land.amortizationMethod+"</td>" +
                      "<td>"+land.amortizationRate+"</td>" +
                      "<td>"+land.amortizationAmount+"</td>" +
                      "<td>"+land.bookValue+"</td>" +
                      "<td>"+land.returnValue+"</td>" +
                      "<td>"+land.scrapValue+"</td>" +
                      "<td>"+land.controlAccount+"</td>" +
                      "<td>"+land.adjustmentAccount+"</td>" +
                      "<td>"+land.shrinkageAccount+"</td>" +
                      "<td>"+land.priceVariance+"</td>" +
                      "<td>"+land.currencyVariance+"</td>" +
                      "<td>"+land.purchasePriceVariance+"</td>" +
                      "<td>"+land.receiptVariance+"</td>" +
                      "<td>"+land.criticalityAsset+"</td>" +
                      "<td>"+land.sinceDate+"</td>" +
                      "<td>"+land.acquisitionStatus+"</td>" +
                      "<td>"+land.ownerShipTransferStatus+"</td>" +
                      "<td>"+land.requestTransferName+"</td>" +
                      "<td>"+land.requiredDate+"</td>" +
                      "<td>"+land.legalFormalityStatus+"</td>" +
                      "<td>"+land.assetSparesName+"</td>" +
                      "<td>"+land.assetSparesComment+"</td>" +
                      "<td>"+land.acquisitionComments+"</td>" +
                      "<td>"+land.createdPerson+"</td>" +
                      "<td>"+land.employeeId+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'createAssetOthers');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.getEmployees=function () {
               $http({
                   method: 'GET',
                   url: 'api/Employees?filter={"where":{"status":"Active"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   $scope.loadingImage=false;
                   console.log('Employees Response :' + JSON.stringify(response));

                   $scope.employeeList = response
                   //$scope.createEmployee = false;
               }).error(function (response) {
                   console.log('Error Response :' + JSON.stringify(response));
               });
           }

           $scope.getEmployees();

           $scope.getCostCenterData=function () {

               $http({
                   method: 'GET',
                   url: 'api/CostCenters?filter={"where":{"status":"Active"}}',
                   /*url: 'http://localhost:8866/api/CostCenters',*/
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('Cost Center  Response :' + JSON.stringify(response));
                   $scope.costCenterList = response;
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }
           $scope.getCostCenterData();

           $scope.getAssetStatusData=function () {

               $http({
                   method: 'GET',
                   url: 'api/AssetStatuses?filter={"where":{"status":"Active"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('Users Response :' + JSON.stringify(response));
                   $scope.assetStatusList = response;
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }
           $scope.getAssetStatusData();

           $scope.getDocumentListMasterData=function () {

               $http({
                   method: 'GET',
                   url: 'api/DocumentListMasters?filter={"where":{"status":"Active"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('Doc List Master Response :' + JSON.stringify(response));
                   $scope.docTypeList = response;
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }

           $scope.getDocumentListMasterData();


           $scope.getMaintenanceObjectiveData=function () {

               $http({
                   method: 'GET',
                   url: 'api/MaintenanceObjectives?filter={"where":{"status":"Active"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('Maintanence Response :' + JSON.stringify(response));
                   $scope.assetTypeList = response;
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }
           $scope.getMaintenanceObjectiveData();

           $scope.getAssetKPIData=function () {

               $http({
                   method: 'GET',
                   url: 'api/AssetKPIs?filter={"where":{"status":"Active"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('KPI Response :' + JSON.stringify(response));
                   $scope.assetKPIList = response;
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }

           $scope.getAssetKPIData();

           $scope.getAssetSparesData=function () {

               $http({
                   method: 'GET',
                   url: 'api/AssetSpares?filter={"where":{"status":"Active"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('AssetSpares Response :' + JSON.stringify(response));
                   $scope.assetSparesList = response;
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }
           $scope.getAssetSparesData();

           $scope.getAssetRiskCriticality=function () {

               $http({
                   method: 'GET',
                   url: 'api/AssetRiskCriticalities?filter={"where":{"status":"Active"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('Asset Criticality Response :' + JSON.stringify(response));
                   $scope.assetCriticalityList = response;
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }
           $scope.getAssetRiskCriticality();


           $scope.getTypeOfDutyData=function () {

               $http({
                   method: 'GET',
                   url: 'api/TypeOfDuties?filter={"where":{"status":"Active"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('Type of Duty Response :' + JSON.stringify(response));
                   $scope.dutyTypeList = response;

                   //console.log("duty list:::" +JSON.stringify(response.duty));
                   //$window.localStorage.setItem('dutyValue', $scope.selectlandTypeId);
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }
           $scope.getTypeOfDutyData();

           //$scope.dutyValue={};

         $scope.dutyChange=function(dutyType)
             {
             console.log("type::"+JSON.stringify(dutyType.dutyType));
             if(JSON.stringify(dutyType.dutyType)==undefined)
              {
               $scope.duty="";
              }
                $scope.dutyValue=JSON.parse(dutyType.dutyType);
                //$scope.dutyValue1=typeof(dutyType);
                console.log("duty type is:::" +JSON.stringify($scope.dutyValue.duty));
                $scope.duty=$scope.dutyValue.duty;
                //console.log("duty type is:::" +$scope.dutyValue1);
             }

           $scope.getFailureClassData=function () {

               $http({
                   method: 'GET',
                   url: 'api/FailureClasses?filter={"where":{"status":"Active"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('Failure Class Response :' + JSON.stringify(response));
                   $scope.assetFailureList = response;
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }
           $scope.getFailureClassData();


           $scope.getAssetStatusData=function () {

               $http({
                   method: 'GET',
                   url: 'api/AssetStatuses?filter={"where":{"status":"Active"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('Users Response :' + JSON.stringify(response));
                   $scope.assetStatusList = response;
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }
           $scope.getAssetStatusData();


           $scope.historicCostsAvailability = ["Yes", "No"];

           $scope.historicCostChanges = function (historicCostAvailability)
           {

               if(historicCostAvailability =="Yes")
               {
                   $scope.historicCost=true;
                   $scope.initialValuationCost=false;

               }
               else {
                   $scope.historicCost=false;
                   $scope.initialValuationCost=true;

               }

           }

           $scope.assetRenewableAvailability = ["Yes", "No"];

           $scope.assetRenewableAvailabilityChanges = function (assetRenewableAvailability)
           {
               //$scope.assetRenewableDate=false;

               if(assetRenewableAvailability =="Yes")
               {
                   $scope.assetRenewableDate=true;

               }
               else {
                   $scope.assetRenewableDate=false;

               }

           }

           $scope.names = ["Owned", "Managed"];

           $scope.selectLandAssetType=function (assetMode) {

               if(assetMode=="Owned")
               {
                   $scope.ownedMode=true;
                   $scope.managedMode=false;
               }
               else if(assetMode="Managed")
               {
                   $scope.ownedMode=false;
                   $scope.managedMode=true;
               }

           }


           $scope.statusChange=function (assetStatusList) {

               $scope.commissionedDate=false;
               $scope.disposalFiles=false;
               $scope.disposalStatus=false;
               $scope.disposalMethod=false;
               $scope.disposalAndDecommissionDate=false;


               if (assetStatusList=="disposal"){

                   $scope.disposalFiles=true;
                   $scope.disposalStatus=true;
                   $scope.disposalMethod=true;
                   $scope.disposalAndDecommissionDate=true;
               }

               else if (assetStatusList=="decommissioned"){

                   $scope.disposalAndDecommissionDate=true;
               }

               else if(assetStatusList=="commissioned")
               {
                   $scope.commissionedDate=true;
               }

           }

      /*     $scope.statusChange=function (assetStatusList) {

               //alert("hello");


               if(assetStatusList=="Disposal" && assetStatusList=="Decommissioned")
               {
                   console.log("date is true");
                   $scope.disposalAndDecommissionDate=true;
               }
               else if (assetStatusList=="Disposal"){

                   console.log("disposal values are true");
                   $scope.disposalFiles=true;
                   $scope.disposalStatus=true;
                   $scope.disposalMethod=true;
               }

               else {
                   console.log("disposal values are false");
                   $scope.disposalAndDecommissionDate=false;
                   $scope.disposalFiles=false;
                   $scope.disposalStatus=false;
                   $scope.disposalMethod=false;
               }

           }*/

           $scope.getAssetTypeData=function () {

               $http({
                   method: 'GET',
                   //url: 'api/AssetTypes?filter=%7B%22where%22%3A%7B%22name%22%3A%20%7B%22neq%22%3A%22Land%22%7D%7D%7D',
                   url: 'api/AssetTypes?filter={"where":{"and":[{"name": {"neq":"land"}},{"status": "Active"}]}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('Users Response :' + JSON.stringify(response));
                   $scope.assetTypeList = response;
                   $scope.loadingImage=false;
               }).error(function (response) {
                   if(response.error.details.messages.name) {
                       $scope.errorMessageData=response.error.details.messages.name[0];
                       $scope.errorMessage=true;
                   }

               });
           }

           $scope.getAssetTypeData();

           $scope.getLandDetails = function () {
               $http({
                   "method": "GET",
                   "url": 'api/AssetForLands?filter=%7B%22where%22%3A%7B%20%22assetType%22%3A%20%7B%22neq%22%3A%22land%22%7D%7D%7D',
                   //"url": 'api/AssetForLands?filter={"where":{"and":[{"assetType": {"neq":"Land"}},{"status": "Active"}]}}',
                   "headers": {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   console.log('create Asset Get Response :' + JSON.stringify(response));
                   $rootScope.landData = response;
                   $scope.createLand = false;


               }).error(function (response, data) {
                   console.log("failure");
               });
           };
           $scope.getLandDetails();

           $scope.showLand = true;
           $scope.createLand = false;
           $scope.editLand=false;
           $scope.showCreateLand = function() {
               $scope.land = {};
               $scope.upload={};
               $scope.loadingImage=false;
               $scope.createLand = true;
               $scope.showLand = false;
               $scope.fileUploadSuccess='';
               $scope.files = [];
               $scope.docList = [];
           }

           $scope.clickToView=function () {
               $scope.loadingImage=false;
               $scope.showLand = true;
               $scope.createLand = false;
               $scope.editLand=false;
               $scope.fileUploadSuccess='';
               $scope.files = [];
               $scope.docList = [];
               $scope.invalidFormat=false;
               $scope.successUpload=false;
           }


           $scope.reset = function () {
               $scope.land = angular.copy($scope.master);
           };

           $scope.land = {};
           $scope.upload={};

           var assetLandCreationForm=true;

           $scope.assetType="land";

           $scope.docTypeSelect=function(documentType)

           {
               console.log("check doc type::" +JSON.stringify(documentType));
               $scope.documentSelectUpload=documentType;
               $('#add2').show();
               $('#add1').hide();
           }

           //*********************************File Upload for Create Asset************************************


           //******file Upload***********
           var formIdsArray = [];
                      var formUploadStatus = false;
                      $scope.disable = true;
                      $scope.docList = [];
                      $scope.uploadDocuments = function (files) {

                          $('#add2').hide();
                          $('#add1').show();
                          formIdsArray = [];
                          $scope.forms = files;
                          var fileCount = 0;

                          angular.forEach(files, function (file) {
                              $scope.disable = true;
                              $scope.errorMssg1 = true;
                              fileCount++;
                              if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                                  formUploadStatus = false;
                                  file.upload = Upload.upload({
                                      url: 'api/Uploads/dhanbadDb/upload',
                                      data: {file: file}
                                  });

                                  $scope.typeOfDocs = {"typeOfDocs": ''};

                                  file.upload.then(function (response) {
                                      $timeout(function () {
                                          $scope.typeOfDocs = response.data;
                                          $scope.typeOfDocs.typeOfDocs = $scope.documentSelectUpload;

                                          console.log("upload doc response::" + JSON.stringify($scope.typeOfDocs));
                                          formIdsArray.push(response.data);
                                          $scope.docList.push(response.data);
                                          formUploadStatus = true;
                                          //alert('Entered')
                                          //file['typeOfDoc']=response.data;
                                           checkStatus();
                                          file.result = response.data;
                                      });
                                  }, function (response) {
                                      if (response.status > 0)
                                          $scope.errorMsg = response.status + ': ' + response.data;
                                  }, function (evt) {
                                      file.progress = Math.min(100, parseInt(100.0 *
                                          evt.loaded / evt.total));
                                  });
                              }else{
                                   formUploadStatus = false;
                                    $scope.successUpload=true;
                                   checkStatus();
                                  //alert('Please Upload JPEG or PDF files only');
                                  console.log("check" +$scope.invalidFormat);
                              }

                               function checkStatus(){
                                  if (fileCount == files.length) {
                                      $scope.disable = false;
                                      $scope.errorMssg1 = false;
                                       $scope.invalidFormat = false;
                                      // $scope.successUpload=false;
                                      //alert(formUploadStatus);
                                      if(formUploadStatus){
                                         $scope.fileUploadSuccess = true;
                                           $scope.successUpload=true;
                                      }else{
                                          $scope.invalidFormat = true;
                                          $scope.fileUploadSuccess = false;
                                            $scope.successUpload=true;
                                      }
                                  }
                              }

                          });
                      };
      //******file Upload***********

           $scope.deleteFile = function(index, fileId){

             if($scope.docList.length==1)
                        {
                        $scope.successUpload=false;
                        }

           $scope.fileUploadSuccess = false;

               $http({
                   method: 'DELETE',
                   url: 'api/Uploads/dhanbadDb/files/'+fileId,
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {
                   $scope.docList.splice(index, 1);
               });

           };

                        $http({
                        method: 'GET',
                        url: 'api/AssetDepartments?filter={"where":{"status":"Active"}}',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"}
                        }).success(function (response) {
                            //console.log('Users Response :' + JSON.stringify(response));
                            $scope.departmentList = response;
                        }).error(function (response) {

                        });

                        $scope.getAccountDetails=function () {

                                 $http({
                                     method: 'GET',
                                     url: 'api/accountDetails?filter={"where":{"status":"Active"}}',
                                     headers: {"Content-Type": "application/json", "Accept": "application/json"}
                                 }).success(function (response) {
                                     console.log('getAccountDetails :' + JSON.stringify(response));
                                     $scope.loadingImage=false;
                                     $scope.accountDetails = response;
                                 }).error(function (response) {
                                     /*if(response.error.details.messages.name) {
                                         $scope.errorMessageData=response.error.details.messages.name[0];
                                         $scope.errorMessage=true;
                                     }*/

                                 });
                             }

                             $scope.getAccountDetails();



                       $scope.getCharterDetails=function () {
                               $http({
                                   method: 'GET',
                                   url: 'api/ProjectCharters',
                                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
                               }).success(function (response) {
                                  console.log('charter Response :' + JSON.stringify(response));
                                   $scope.charterList=response;
                                   //    //alert('successfully created charter');
                               }).error(function (response) {
                                   console.log('Error Response :' + JSON.stringify(response));
                               });
                               ////alert('get charter list');
                           }
                       $scope.getCharterDetails();



                       $scope.askDetails=function()

                                 {

                                  console.log("yes button");
                                   $('#askModal').modal('hide');
                                   $('#askModel1').modal('show');
                                 }


                                 $scope.withOutAskDetails=function()
                                 {

                                  console.log("no button");
                                   $('#askModal').modal('hide');
                                   $('#askModel1').modal('hide');

                                   console.log("asset creation");

                                   if($scope.ownedMode==true)
                                   {
                                     $scope.deptData = [];
                                     $scope.deptData.push(JSON.parse($scope.land.department));
                                     $scope.land.department=null;
                                     $scope.land.department = $scope.deptData[0].id;
    //console.log("0000000000000000000000000000000000  "+JSON.stringify($scope.land.department))
    //                                                      for(var i=0;i<$scope.deptData.length;i++){
    //                                                          $scope.land.department=$scope.deptData[i].id;
    //                                                      }
    //
    //                                                      console.log(JSON.stringify($scope.land.department))
                                   }

                                                  var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                                                  var land=$scope.land;
                                                  land['createdPerson']=loginPersonDetails.name;
                                                  land['employeeId']=loginPersonDetails.employeeId;
                                                  //land['assetType']=$scope.assetType;
                                                  land.file = $scope.docList;
                                                  land['duty']=$scope.duty;
                                                  if(assetLandCreationForm) {
                                                      assetLandCreationForm = false;
                                                      $http({
                                                          method: 'POST',
                                                          url: 'api/AssetForLands',
                                                          headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                                          data: land
                                                      }).success(function (response) {

                                                          console.log('Land Response: :' + JSON.stringify(response));
                                                          $scope.docList = [];
                                                          $scope.getEmployees();
                                                          $scope.createLand = false;
                                                          $scope.showLand = true;

                                                          $scope.getLandDetails();
                                                          console.log("create asset details: " + JSON.stringify($scope.land));
                                                          $("#addEmployeeSuccess").modal("show");
                                                          setTimeout(function () {
                                                              $('#addEmployeeSuccess').modal('hide')
                                                          }, 3000);

                                                          $rootScope.landData.push(response);
                                                          console.log("$rootScope.landData*************" + JSON.stringify($rootScope.landData));

                                                          //$scope.selectLandType=response.landType;
                                                         /* $scope.selectlandTypeId = response.id;
                                                          $window.localStorage.setItem('landCreationCheck', $scope.selectlandTypeId);*/

                                                          //$scope.selectCaseNumber=response.caseNumber;

                                                          //$scope.landAndAssetUploads();

                                                          //location.href = '#/landDetails';
                                                          //$scope.land = {};
                                                          //$('#myModal').modal('hide');
                                                          //
                                                      }).error(function (response) {
                                                          console.log('Error Response1 :' + JSON.stringify(response));

                                                      });
                                                  }

                                              };


                                 $scope.charter={};


                      // var land={};

                      // var projectArray=[];




                       $scope.createProjectPlan=function()
                                 {
                                     console.log("land creation");

                                     var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                                     var land=$scope.land;
                                     land['createdPerson']=loginPersonDetails.name;
                                     land['employeeId']=loginPersonDetails.employeeId;
                                     //land['assetType']=$scope.assetType;
                                     land.file = $scope.docList;
                                     land['duty']=$scope.duty;
                                     land['projectDetails']=$scope.charter;
                                     if($scope.ownedMode==true)
                                         {
                                           $scope.deptData = [];
                                           $scope.deptData.push(JSON.parse($scope.land.department));
                                           $scope.land.department=null;
                                           $scope.land.department = $scope.deptData[0].id;
//console.log("0000000000000000000000000000000000  "+JSON.stringify($scope.land.department))
//                                                      for(var i=0;i<$scope.deptData.length;i++){
//                                                          $scope.land.department=$scope.deptData[i].id;
//                                                      }
//
//                                                      console.log(JSON.stringify($scope.land.department))
                                         }


                                               /*    console.log("charter info::" +JSON.stringify($scope.charter))
                                                   var charter=$scope.charter;
                                                   charter['assetType']=$scope.assetType;
                                                   charter.land=$scope.land;
                                                   console.log("@@@@@@@@@@@@@@@@@@users response@@@@@@@@@@@@@@"+JSON.stringify($scope.charter));
           */
                                                       $http({
                                                           method: 'POST',
                                                           url: 'api/AssetForLands',
                                                           headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                                           data: land
                                                       }).success(function (response) {

                                                          $scope.landPostSuccess=response;

                                                           //createLandButton();
                                                           console.log('Land Response: :' + JSON.stringify(response));
                                                           $('#askModel1').modal('hide');
                                                           $scope.docList = [];
                                                           $scope.getEmployees();
                                                           $scope.createLand = false;
                                                           $scope.showLand = true;
                                                           $rootScope.landData.push(response);
                                                           console.log("$rootScope.landData*************" + JSON.stringify($rootScope.landData));

                                                           $scope.getLandDetails();
                                                           console.log("create asset details: " + JSON.stringify($scope.land));
                                                           $("#addEmployeeSuccess").modal("show");
                                                           setTimeout(function () {
                                                               $('#addEmployeeSuccess').modal('hide')
                                                           }, 3000);

                                                           //$scope.selectLandType=response.landType;
                                                           $scope.selectlandTypeId = response.id;
                                                           $window.localStorage.setItem('landCreationCheck', $scope.selectlandTypeId);

                                                           //$scope.selectCaseNumber=response.caseNumber;

                                                           //$scope.landAndAssetUploads();

                                                           //location.href = '#/landDetails';
                                                           //$scope.land = {};
                                                           //$('#myModal').modal('hide');

                                                       }).error(function (response) {
                                                           console.log('Error Response1 :' + JSON.stringify(response));

                                                       });


                                               };


           $scope.createLandButton = function () {
               console.log("asset creation");
               // alert('data'+$scope.land.dutyType.name);
//               var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
//               var land=$scope.land;
//               land['createdPerson']=loginPersonDetails.name;
//               land['employeeId']=loginPersonDetails.employeeId;
//               //land['landType']=$scope.landType;
//               land.file = $scope.docList;
//               land['duty']=$scope.duty;
//               var dataIs=JSON.parse($scope.land.dutyType);
//                    land['dutyType']=dataIs.name;
//               console.log("@@@@@@@@@@@@@@@@@@users response@@@@@@@@@@@@@@"+JSON.stringify($scope.land));
//
//                console.log("land creation");

                              var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                              var land=$scope.land;
                              land['createdPerson']=loginPersonDetails.name;
                              land['employeeId']=loginPersonDetails.employeeId;
                              //land['assetType']=$scope.assetType;
                              land.file = $scope.docList;
                              land['duty']=$scope.duty;
                              $("#askModal").modal("show");
                              var dataIs=JSON.parse($scope.land.dutyType);
                              land['dutyType']=dataIs.name;

                              console.log("@@@@@@@@@@@@@@@@@@users response@@@@@@@@@@@@@@"+JSON.stringify($scope.land));


               /*if(assetLandCreationForm) {
                   assetLandCreationForm = false;
                   $http({
                       method: 'POST',
                       url: 'api/AssetForLands',
                       headers: {"Content-Type": "application/json", "Accept": "application/json"},
                       data: land
                   }).success(function (response) {

                       console.log('Land Response: :' + JSON.stringify(response));
                       $scope.docList = [];
                       $scope.getEmployees();
                       $scope.createLand = false;
                       $scope.showLand = true;
                       $rootScope.landData.push(response);
                       console.log("$rootScope.landData*************" + JSON.stringify($rootScope.landData));

                       $scope.getLandDetails();
                       console.log("create asset details: " + JSON.stringify($scope.land));
                       $("#addEmployeeSuccess").modal("show");
                       setTimeout(function () {
                           $('#addEmployeeSuccess').modal('hide')
                       }, 3000);

                       //$scope.selectLandType=response.landType;
                       $scope.selectlandTypeId = response.id;
                       $window.localStorage.setItem('landCreationCheck', $scope.selectlandTypeId);

                       //$scope.selectCaseNumber=response.caseNumber;

                       //$scope.landAndAssetUploads();

                       //location.href = '#/landDetails';
                       //$scope.land = {};
                       //$('#myModal').modal('hide');
                   }).error(function (response) {
                       console.log('Error Response1 :' + JSON.stringify(response));

                   });
               }*/

           };

           //***********select*************


           $(function () {
               $("#scroll2").hide();
               $("#scroll").click(function () {
                   $("#scroll2").show();
                   $("#scroll").hide();
                   $('html,body').animate({
                           scrollTop: $("#scroll2").offset().top
                       },
                       'slow');
               });
               $("#cancel").click(function () {
                   $("#scroll2").hide()
                   $("#scroll").show();
                   ;
               });
           });

           //******************* File Upload For Create Asset *****************************************


           $scope.editLandDetails={};
           $scope.editLandData=function (landDetails) {

               $scope.loadingImage=false;
               $scope.editLandDetails=landDetails;
               $scope.editLand=true;
               $scope.showLand = false;

               console.log(" editEmployeeDetails entered  " +JSON.stringify($scope.editLandDetails));
           }
           var addAssetTypeSubmit = true;
           $scope.editLandButton=function () {
               $scope.loadingImage=false;

               $scope.empUpdationError = false;
               $scope.updateEmpError = '';

               var editLandDetails= $scope.editLandDetails;
               var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
               editLandDetails['lastEditPerson']=loginPersonDetails.name;

               if(addAssetTypeSubmit)
               {
               addAssetTypeSubmit = false;
               $http({
                   "method": "PUT",
                   "url": 'api/AssetForLands/'+$scope.editLandDetails.id,
                   "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                   "data": editLandDetails
               }).success(function (response, data) {
                   $scope.createLand = false;
                   $scope.editLand=false;
                   $scope.showLand = true;
                   $scope.getLandDetails();
                   console.log("update land details: "+JSON.stringify($scope.editLandDetails) );
                   $("#editEmployeeSuccess").modal("show");
                   setTimeout(function(){$('#editEmployeeSuccess').modal('hide')}, 3000);
               }).error(function (response, data) {
                   console.log("failure");

                   if(response.error.details.messages.email) {
                       $scope.updateEmpError=response.error.details.messages.email[0];
                       $scope.updateEmpError = 'Email already Exist';
                       $scope.empUpdationError = true;
                   }
                   if(response.error.details.messages.employeeId) {
                       $scope.updateEmpError=response.error.details.messages.employeeId[0];
                       $scope.updateEmpError = 'EmployeeId already Exist';
                       $scope.empUpdationError = true;
                   }
                   $timeout(function(){
                       $scope.empUpdationError=false;
                   }, 3000);
               })

               }
               addAssetTypeSubmit = true;
           }
           $scope.getLandDetails();

  });